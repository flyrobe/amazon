/**
 * FLYROBE - Responsive Dashboard
 *
 * FLYROBE theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, IdleProvider, KeepaliveProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $urlRouterProvider.otherwise("/login");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

        .state('dashboards', {
            abstract: true,
            url: "/dashboards",
            templateUrl: "views/common/content.html?v=20190306-1"
        })
        .state('login', {
            url: "/login",
            templateUrl: "views/login.html?v=20190306-1",
            data: { pageTitle: 'AMAZON'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe', {
            abstract: true,
            url: "/flyrobe",
            templateUrl: "views/common/content.html?v=20190306-1"
        })
        .state('flyrobe.invites', {
            url: "/invites",
            templateUrl: "views/invite/invite.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.manufacture', {
            url: "/merchant",
            templateUrl: "views/manufacture/manufacture.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.createManufacture', {
            url: "/create-merchant",
            templateUrl: "views/manufacture/create-manufacture.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.catalog', {
            url: "/catalog",
            templateUrl: "views/catalog/catalog-list.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.createCatalog', {
            url: "/create-catalog",
            templateUrl: "views/catalog/create-catalog.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.createBulkCatalog', {
            url: "/create-bulk-catalog",
            templateUrl: "views/catalog/bulk-upload-catalog.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.itemList', {
            url: "/item-list",
            templateUrl: "views/catalog/item-list.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.vendor', {
            url: "/seller",
            templateUrl: "views/vendor/vendor-list.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.createVendor', {
            url: "/create-seller",
            templateUrl: "views/vendor/create-vendor.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.consignment', {
            url: "/consignment-list",
            templateUrl: "views/consignment/consignment-list.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.returnConsignmentList', {
            url: "/return-consignment-list",
            templateUrl: "views/consignment/return-consignment-list.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.createConsignment', {
            url: "/create-consignment",
            templateUrl: "views/consignment/create-consignment.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.returnConsignment', {
            url: "/create-return-consignment",
            templateUrl: "views/consignment/return-consignment.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.sellerConsignments', {
            url: "/seller-consignments",
            templateUrl: "views/consignment/seller-consignments.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.sellerReturnConsignments', {
            url: "/seller-return-consignments",
            templateUrl: "views/consignment/seller-return-consignments.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.picklist', {
            url: "/to",
            templateUrl: "views/picklist/picklist.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.createPicklist', {
            url: "/create-to",
            templateUrl: "views/picklist/create-picklist.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.createPickList', {
            url: "/create-tranferred-order",
            templateUrl: "views/picklist/create-pick-list.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.trunkshow', {
            url: "/trunkshow",
            templateUrl: "views/trunkshow/trunkshow.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                }
            }
        })
        .state('flyrobe.createTrunkshow', {
            url: "/create-trunkshow",
            templateUrl: "views/trunkshow/create-trunkshow.html?v=20190306-1",
            data: { pageTitle: 'AMAZON' ,specialClass:"mini-navbar"},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngDialog',
                            files: ['css/plugins/ngDialog/ngDialog-theme-default.min.css',
                                'css/plugins/ngDialog/ngDialog.min.css',
                                'js/plugins/ngDialog/ngDialog.min.js']
                        },
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        }
                    ]);
                }
            }
        })
        .state('recover', {
            url: "/recover-password",
            templateUrl: "views/pages/forgot-password.html?v=20190306-1",
            data: { pageTitle: 'AMAZON'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/parsley/parsley.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js',
                                'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                    ]);
                }
            }
        })
    ;

}
angular
    .module('flyrobe')
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;

        //function to get previous state
        /*$rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
            mainVm.previousState = from.name;
        });*/


    });
