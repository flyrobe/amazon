
/**
 * MainCtrl - controller
 * Contains several global data used in different view
 *
 */
function MainCtrl($rootScope, $http, $localStorage, $state, $scope, $filter, ngDialog, mainServices) {
    mainVm = this;

    /**
     * slideInterval - Interval for bootstrap Carousel, in milliseconds:
     */
    this.slideInterval = 5000;
    mainVm.lastDate = new Date("June 14, 2017");
    mainVm.todayDate = $filter('date')(new Date(), 'yyyy-MM-dd');
    mainVm.today_date_time = $filter('date')(new Date(), 'yyyy-MM-dd HH:mm');
    mainVm.version = "20-08-2018_1.0";

    mainVm.logout                       = logout; //function to logout
    mainVm.collaspe                     = collaspe; // function to show/hide the i-box
    mainVm.logoutFromOtherDevices       = logoutFromOtherDevices;


    var url              = "https://ops.flyrobeapp.com/";//prod
    // var url              = "http://api-master.flyrobeapp.com/";
     // var url                 = "http://staging.flyrobeapp.com/";//staging

    mainVm.api                 = url + "api/web/v1/";
    mainVm.api_v2              = url + "api/web/v2/";
    mainVm.api_master          = "http://api-master.flyrobeapp.com/api/web/v1/";

    //Prod Log Token
    mainVm.log_token = '12dd3tgk4fji00y6qy';

    //Staging Log token
    // mainVm.log_token = '1nhf2f48djh05mnd8';

    mainVm.urlList          = {
                                    "node_url_2" : "https://node-web.flyrobeapp.com/",//prod
                                    "order_url" : "https://node.flyrobeapp.com:8002/",//prod
                                    "qc_url" : "https://node.flyrobeapp.com:8081/",//prod
                                    "website_url" : "https://flyrobe.com/",//prod
                                    "node_Db_url" : "http://node.flyrobeapp.com:8002/orderItemInfoNewDB",//prod
                                    "logs_url_1" : "https://ops-logs.flyrobeapp.com/api/v1/",//prod
                                    "logs_url_2" : "https://ops-logs.flyrobeapp.com/api/v2/",//prod
                                    "pufm_url" : "https://merchant.flyrobeapp.com/",//prod
                                     "flymall" : 'https://node.flyrobeapp.com:8002/api/auth/flymall/',//prod
                                     "flymall2" : 'https://node.flyrobeapp.com:8002/flymall/',//prod

                                    // "node_url_2" : "http://node-staging.flyrobeapp.com:8001/",//staging
                                    // "order_url" : "https://node-staging.flyrobeapp.com:8002/",//staging
                                    // "qc_url" : "https://node-staging.flyrobeapp.com:8081/",//staging
                                    // "website_url" : "http://alpha.flyrobeapp.com/",//staging
                                    // "node_Db_url" : "http://node-staging.flyrobeapp.com:8002/orderItemInfoNewDB",//staging
                                    // "logs_url_1" : "http://node-staging.flyrobeapp.com:3000/api/v1/",//staging
                                    // "logs_url_2" : "http://node-staging.flyrobeapp.com:3000/api/v2/",//staging
                                    // "pufm_url" : "http://node-staging.flyrobeapp.com:8080/",//staging
                                    // "flymall" : 'https://node-staging.flyrobeapp.com:8002/api/auth/flymall/',//staging
                                    // "flymall2" : 'https://node-staging.flyrobeapp.com:8002/flymall/',//staging

                                    "dashboard_url" : url + "api/dashboard/v1/",
                                    "dashboard_url_v2" : url + "api/dashboard/v2/",
                                    "store_api_v1" : url + "api/store/v1/",
                                    "store_api_v2" : url + "api/store/v2/",
                                    "url": "https://central.flyrobeapp.com/temp_o/",
                                    "size_guide_url": "https://s3-ap-southeast-1.amazonaws.com/flyrobe.user.logs/parent_item_dummy_images/17-Oct-2018-14-54-47",
                                    "node_url" : "http://node.flyrobeapp.com/",
                                    "log_url": "http://node.flyrobeapp.com:8220/",
                                    "delhivery_track_url" : "https://cl.delhivery.com/#/package/",
                                    "delhivery_track_url_2" : "https://www.delhivery.com/track/#!/package/",
                                    "xpressbees_track_url" : "http://www.xpressbees.com/Admin.aspx#",
                                    "shadowfax_track_url" : "",
                                    "shadowfax_track_url_2" : "https://track.shadowfax.in/track?order=new&trackingId=",
                                    "dhl_track_url" : "https://s3-ap-southeast-1.amazonaws.com/flyrobe.user.logs/dhl_invoice/",
                                    "cloundnary_url" : "https://res.cloudinary.com/dvi1cncba/",
                                    "cloundnary_url_2" : "https://res.cloudinary.com/dvi1cncba/image/upload/",
                                    "imagekit_url" : "https://ik.imagekit.io/flyrobe/",
                                    "imagekit_100_url" : "http://ik.imagekit.io/flyrobe/tr:w-50/",
                                    "imagekit_120_url" : "http://ik.imagekit.io/flyrobe/tr:w-120/",
                                    "refund_url" : mainVm.api,
                                    "api_master" : "http://api-master.flyrobeapp.com/api/web/v1/",
                                    "noImageUrl" : "https://flyrobe.user.logs.s3.amazonaws.com/parent_item_dummy_images/13-Sep-2017-15-01-65"
                              };
    mainVm.headers          = {
                                 'Authorization' : 'Bearer dashboard',
                                 'Content-Type'  : 'application/json'
                              };

    mainVm.cities  = [
        {"id" : 1, "name" : "Mumbai",       "parentCity" : false},
        {"id" : 2, "name" : "Delhi",        "parentCity" : true},
        {"id" : 3, "name" : "Ahmedabad",    "parentCity" : false},
        {"id" : 4, "name" : "Hyderabad",    "parentCity" : false},
        {"id" : 5, "name" : "Bangalore",    "parentCity" : false},
        {"id" : 6, "name" : "Pune",         "parentCity" : false},
        {"id" : 7, "name" : "Chandigarh",   "parentCity" : false},
        {"id" : 8, "name" : "Chennai",      "parentCity" : false},
        {"id" : 9, "name" : "Jaipur",       "parentCity" : false},
        {"id" : 10, "name" : "Kolkata",     "parentCity" : false},
        {"id" : 11, "name" : "Faridabad",   "parentCity" : false},
        {"id" : 12, "name" : "Ghaziabad",   "parentCity" : false},
        {"id" : 13, "name" : "Goa",         "parentCity" : false},
        {"id" : 14, "name" : "Ludhiana",    "parentCity" : false},
        {"id" : 15, "name" : "Lucknow",     "parentCity" : false},
        {"id" : 16, "name" : "Indore",      "parentCity" : false},
        {"id" : 17, "name" : "Noida",       "parentCity" : false},
        {"id" : 18, "name" : "Agra",        "parentCity" : false},
        {"id" : 19, "name" : "Kanpur",      "parentCity" : false},
        {"id" : 20, "name" : "Nagpur",      "parentCity" : false},
        {"id" : 21, "name" : "Patna",       "parentCity" : false},
        {"id" : 22, "name" : "Bhopal",      "parentCity" : false},
        {"id" : 23, "name" : "Surat",       "parentCity" : false},
        {"id" : 24, "name" : "Vadodara",    "parentCity" : false},
        {"id" : 25, "name" : "Raipur",      "parentCity" : false},
        {"id" : 26, "name" : "Nasik",       "parentCity" : false},
        {"id" : 27, "name" : "Meerut",      "parentCity" : false},
        {"id" : 30, "name" : "Mohali",      "parentCity" : false},//for merchant only
        {"id" : 31, "name" : "Gurgaon",     "parentCity" : false},//for merchant only
        {"id" : 32, "name" : "Mangalore",   "parentCity" : false},//for merchant only
        {"id" : 33, "name" : "Ratlam",      "parentCity" : false},//for merchant only
        {"id" : 34, "name" : "Karnal",      "parentCity" : false},//for merchant only
        {"id" : 35, "name" : "Panchkula",   "parentCity" : false},//for merchant only
        {"id" : 36, "name" : "Varanasi",    "parentCity" : false},//for merchant only
        {"id" : 37, "name" : "Bhubaneshwar","parentCity" : false},//for merchant only
        {"id" : 38, "name" : "Thiruvananthapuram","parentCity" : false},//for merchant only
        {"id" : 39, "name" : "Kochi",       "parentCity" : false},//for merchant only
        {"id" : 40, "name" : "Kodambakkam", "parentCity" : false},//for merchant only
        {"id" : 41, "name" : "Bijapur",     "parentCity" : false},//for merchant only
        {"id" : 42, "name" : "Dwarka",      "parentCity" : false},//for merchant only
        {"id" : 43, "name" : "Kolhapur",    "parentCity" : false},//for merchant only
        {"id" : 44, "name" : "Tiruppur",    "parentCity" : false},//for merchant only
        {"id" : 45, "name" : "Bareilly",    "parentCity" : false},//for merchant only
        {"id" : 46, "name" : "Kota",        "parentCity" : false},//for merchant only
        {"id" : 47, "name" : "Jalandhar",   "parentCity" : false},//for merchant only
        {"id" : 48, "name" : "Rajkot",      "parentCity" : false},//for merchant only
        {"id" : 49, "name" : "Jodhpur",     "parentCity" : false},//for merchant only
        {"id" : 50, "name" : "Navsari",     "parentCity" : false},//for merchant only
        {"id" : 51, "name" : "Junagadh",    "parentCity" : false},//for merchant only
        {"id" : 52, "name" : "Nalagarh",    "parentCity" : false},//for merchant only
        {"id" : 53, "name" : "Sri Ganganagar", "parentCity" : false},//for merchant only
        {"id" : 54, "name" : "Amritsar",    "parentCity" : false},//for merchant only
        {"id" : 55, "name" : "Dehradun",    "parentCity" : false},//for merchant only
        {"id" : 56, "name" : "Secunderabad","parentCity" : false},//for merchant only
        {"id" : 57, "name" : "Udaipur",     "parentCity" : false},//for merchant only
        {"id" : 100, "name" : "All",        "parentCity" : false}
    ];

    mainVm.point_of_contact_list = "heli.mehta@flyrobe.com,harshita.khandelwal@flyrobe.com,aishwarya.singh@flyrobe.com,arpitta.jerath@flyrobe.com," +
        "jitesh.phulwani@flyrobe.com,surbhi.goel@flyrobe.com,palak.bhagat@flyrobe.com,arko.banerjee@flyrobe.com,anshuman.sharma@flyrobe.com,salim.vasaya@flyrobe.com";

    mainVm.majorCities  = [
        {"id" : 2, "name" : "Delhi"}
    ];

    mainVm.logistic_partner_list = [
        { 'id' : 'Delhivery', 'name' : 'Delhivery'},
        { 'id' : 'ShadowFax', 'name' : 'ShadowFax'},
        { 'id' : 'Inhouse', 'name' : 'In-house'},
        { 'id' : 'Any', 'name' : 'Any'}
    ];


    var date = new Date();
    mainVm.todayDate = $filter('date')(date, 'yyyy-MM-dd');
    date.setDate(date.getDate()-1);
    mainVm.currentDate = $filter('date')(date, 'yyyy-MM-dd');

    if($localStorage.fly_amazon){

        if($localStorage.fly_amazon.email)
            mainVm.email = $localStorage.fly_amazon.email;

        if($localStorage.fly_amazon.access_token)
            mainVm.access_token = $localStorage.fly_amazon.access_token;

        if($localStorage.fly_amazon.state)
        mainVm.flyrobeState = $localStorage.fly_amazon.state;

    }


    if($localStorage.fly_amazon && $localStorage.fly_amazon.user_info){

        mainVm.fly_amazon = $localStorage.fly_amazon;
        mainVm.node_header = {
            'Authorization' : 'Bearer ' + $localStorage.fly_amazon.user_info.access_token
        };
        mainVm.node_header_1 = {
            'Authorization' : 'Bearer ' + $localStorage.fly_amazon.user_info.access_token,
            'Content-Type': 'application/x-www-form-urlencoded'
        };

        mainVm.headers          = {
            'Authorization' : 'Bearer ' + $localStorage.fly_amazon.user_info.access_token,
            'Content-Type'  : 'application/json'
        };

    }

    /**********************************************
     function to logout
     **********************************************/
    function logout(){

        delete $localStorage.fly_amazon;
        delete $localStorage.apiflyrobe;
        delete $localStorage.fly_amazon;
        $state.go('login');

    }

    /**********************************************
            function to show/hide the i-box
     **********************************************/
    function collaspe(class_name){
        $("." + class_name).toggle();
    }


    function logoutFromOtherDevices() {
        $http({
            method: 'GET',
            url: mainVm.api + 'access-tokens-delete',
            headers: mainVm.headers
        }).success(function (response) {

            if(response.meta.is_error){
                alert(response.data);
            }
            else{
                alert(response.data);
            }

        }).error(function (response,status) {
            var res = mainServices.checkErrorStatus(status);
            if(res){
                if(status == 404){//404 Access-token do not exist
                    mainServices.checkErrorStatus(401);
                }
            }
        });
    }

}

/**
 *
 * Pass all functions into module
 */
angular
    .module('flyrobe')
    .controller('MainCtrl', MainCtrl)
    .filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});
