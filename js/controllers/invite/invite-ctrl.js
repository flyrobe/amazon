/**
 ********** Created by Surbhi Goel ************
 **/

function inviteCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService) {

    icCtrl  = this;

    icCtrl.image_links = [
        '/v1545049581/Trunk_show_-Blank-Revised_3_2x_ddbcfg.jpg',//simple By Me
        '/v1545049575/Trunk_show_-Blank-Revised_4_2x_dkmfpt.jpg',//simple By Us
        '/v1545049584/Trunk_show_-Blank-Revised_1_2x_xyaptr.jpg',//wtsapp By Me
        '/v1545049585/Trunk_show_-Blank-Revised_2_2x_iwzb0x.jpg' //wtsapp By Us
    ];

    icCtrl.form = {
        name : '',
        type : 'ME'
    };
    icCtrl.show_image = false;
    icCtrl.url1 = "";
    icCtrl.url2 = "";

    icCtrl.getImage         = getImage;


    mainVm.flyrobeState     = "Invitation";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;


    function getImage() {

        icCtrl.show_image = false;

        var add1 = icCtrl.form.add_line1.replace(/,/g, "%252C");
        add1 = add1.replace(/\//g, "%252F");


       /* var url = "c_fit,l_text:Lato_36_center:" + icCtrl.form.name.toUpperCase() +
                ',g_north,w_750,y_265/c_fit,l_text:Lato_30_bold_center:' + icCtrl.form.date + " %252C " + icCtrl.form.time +
                ',g_south,y_300,co_rgb:21252e/c_fit,l_text:Lato_30_center:' + add1;

        if(icCtrl.form.add_line2){
            var add2 = icCtrl.form.add_line2.replace(/,/g, "%252C");
            url  += ',g_south,y_400,co_rgb:21252e/c_fit,l_text:Lato_30_center:' + add2;
        }

        url += ",g_south,y_350,co_rgb:21252e";*/



        if(icCtrl.form.add_line2){
            var url = "c_fit,l_text:Lato_36_center:" + icCtrl.form.name.toUpperCase() +
                ',g_north,w_750,y_265/c_fit,l_text:Lato_30_bold_center:' + icCtrl.form.date + " %252C " + icCtrl.form.time +
                ',g_south,y_325,co_rgb:21252e/c_fit,l_text:Lato_30_center:' + add1;
            var add2 = icCtrl.form.add_line2.replace(/,/g, "%252C");
            add2 = add2.replace(/\//g, "%252F");
            url  += ',g_south,y_425,co_rgb:21252e/c_fit,l_text:Lato_30_center:' + add2;
            url += ",g_south,y_375,co_rgb:21252e";
        }
        else{
            var url = "c_fit,l_text:Lato_36_center:" + icCtrl.form.name.toUpperCase() +
                ',g_north,w_750,y_265/c_fit,l_text:Lato_30_bold_center:' + icCtrl.form.date + " %252C " + icCtrl.form.time +
                ',g_south,y_350,co_rgb:21252e/c_fit,l_text:Lato_30_center:' + add1;
            url += ",g_south,y_400,co_rgb:21252e";
        }

        if(icCtrl.form.type == 'ME'){
            icCtrl.url1 = url + icCtrl.image_links[0];
            icCtrl.url2 = url + icCtrl.image_links[2];
        }
        else{
            icCtrl.url1 = url + icCtrl.image_links[1];
            icCtrl.url2 = url + icCtrl.image_links[3];
        }

        icCtrl.show_image = true;

    }

}

angular
    .module('flyrobe')
    .controller('inviteCtrl', inviteCtrl);