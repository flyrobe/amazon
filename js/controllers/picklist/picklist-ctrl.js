/**
 ********** Created by Surbhi Goel ************
 **/

function picklistCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                         mainServices,ngDialog,DTOptionsBuilder) {

    pl = this;
    pl.panel = "PICK";

    if(!$filter('tab')(pl.panel)) {
        $state.go('login');
    }


    mainVm.flyrobeState     = "Order List";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    pl.getData                          = getData;
    pl.exportToCsv                      = exportToCsv;
    pl.getVendorList                    = getVendorList;
    pl.viewDetails                      = viewDetails;
    pl.confirm                          = confirm;
    pl.disablePicklist                  = disablePicklist;
    pl.log_url                          = log_url;
    pl.get_trigger_time                 = get_trigger_time;
    pl.change_date_representation       = change_date_representation;

    pl.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withDisplayLength(50)
        .withOption('order', [1, 'asc'])
        .withButtons([
            {extend: 'copy'}
        ]);// dataTable Searching , Sorting & pagination

    pl.getVendorList(); //Default Call

    function get_trigger_time(){
        var date = new Date()
        return date.getFullYear()+'-'+pl.change_date_representation(date.getMonth()+1)+'-'+pl.change_date_representation(date.getDate())+','+pl.change_date_representation(date.getHours())+':'+pl.change_date_representation(date.getMinutes())
    };

    function change_date_representation(d) {
        if (d <10){
          return '0'+ d;
        }
        return d;
      };

    function log_url(){
    var log = {
        "platform": "Amazon Dashboard",
        "platform_properties": {
           "_flyrobe_analytics_version": "",
           "_android_os_name": "",
           "_android_os_version": "",
           "_android_manufacturer": "",
           "_android_brand": "",
           "_android_model": "",
           "_network_carrier": "",
           "_network_type": ""
         },
         "user_email": JSON.parse(window.localStorage.getItem('ngStorage-fly_amazon')).email,
         "user_properties": {
           "_user_id": "",
           "_name": "",
           "_first_name": "",
           "_last_name": "",
           "_gender": "",
           "_email": "",
           "_mobile": "",
           "_channel": ""
         },
         "event": "Disable Picklist",
         "event_properties": {
           "picklist_id" : pl.pick_list_id,
           "host_username" : pl.host_user_name,
           "trunkshow_id" : pl.trunk_id ? pl.trunk_id : 'old'
         },
         "version": "1.0.0",
         "trigger_time" : pl.get_trigger_time()
  }

    $http({
        url: mainVm.urlList.logs_url_1 + 'add_event/',
        method: 'POST',
        headers: {
                  'Authorization': mainVm.log_token,
                  'Content-Type': "application/json"
              },
        data: log
    }).success(function (response) {
        console.log("Log Success")
    }).error(function (error) {
        console.log("Some Error")
    });
  }

    function getData() {

        console.log("pl.date_range ", $('#rangeInput').val());

        if($('#rangeInput').val() == "" || $('#rangeInput').val() == undefined || $('#rangeInput').val() == null){
            var url = mainVm.urlList.flymall + 'pick-list/';
        }
        else{
            var date = $('#rangeInput').val().split(' ');
            var url = mainVm.urlList.flymall + 'pick-list/?start_date=' + date[0] + '&end_date=' + date[2];
        }

        pl.spinners     = true;

        $http({
            method: 'GET',
            url: url,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            pl.spinners = false;
            pl.table_data = response.data;

            getTableData();

        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function getTableData() {

      if(pl.pick_list_id){
        for(var i=0; i<pl.table_data.length; i++){
          if(pl.table_data[i].id == pl.pick_list_id){
            pl.table_data.splice(i, 1);
            break;
          }
        }
      }

      if(pl.table_data.length){

          //column def
          {
              var column = [];
              pl.csv_header = [];

              var col_name = {
                  name : 'id',
                  displayName: 'Id',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p">' +
                  '{{COL_FIELD}}<br>' +
                  '<a ng-click="grid.appScope.pl.viewDetails(row.entity.full_info)">' +
                  'View Details' +
                  '</a>' +
                  '</div>'
              };
              column.push(col_name);
              pl.csv_header.push('Id');

              var col_name = {
                  name : 'created_at',
                  displayName: 'Created At',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              pl.csv_header.push('Created At');

              var col_name = {
                  name : 'created_by',
                  displayName: 'Created By',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              pl.csv_header.push('Created By');

              var col_name = {
                  name : 'grand_price',
                  displayName: 'Grand Price',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              pl.csv_header.push('Grand Price');

              var col_name = {
                  name : 'consigning_on',
                  displayName: 'Consigning On',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              pl.csv_header.push('Consigning On');

              var col_name = {
                  name : 'number_of_consignment',
                  displayName: 'No of Consignment',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              pl.csv_header.push('No of Consignment');

              var col_name = {
                  name : 'number_of_items_in_consignment',
                  displayName: 'No of Items in Consignment',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              pl.csv_header.push('No of Items in Consignment');

              var col_name = {
                  name : 'idle_count',
                  displayName: 'Idle Count',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              pl.csv_header.push('Idle Count');

              var col_name = {
                  name : 'destination_vendor',
                  displayName: 'Destination',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              pl.csv_header.push('Destination');

              var col_name = {
                  name : 'comment',
                  displayName: 'Comment',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              pl.csv_header.push('Comment');

              var col_name = {
                  name : 'to_be_received_on',
                  displayName: 'To Be Received On',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              pl.csv_header.push('To Be Received On');

              var col_name = {
                  name : 'disable_picklist',
                  displayName: 'Disable Picklist',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p">' +
                  '<button class="btn export-btn picklist-disable-btn" ng-click="grid.appScope.pl.confirm(row.entity.full_info)">' +
                  'Disable Picklist' +
                  '</button>' +
                  '</div>'
              };
              column.push(col_name);
              // pl.csv_header.push('Disable Picklist');

          }

          pl.gridOptionsComplex = {
              enableFiltering: true,
              showGridFooter: true,
              showColumnFooter: true,
              paginationPageSizes: [10, 25, 50,100],
              paginationPageSize: 100,
              rowHeight: 100,
              columnDefs: column,
              exporterCsvFilename: 'myFile.csv',
              exporterPdfDefaultStyle: {fontSize: 9},
              exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
              exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
              exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
              exporterPdfFooter: function ( currentPage, pageCount ) {
                  return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
              },
              exporterPdfCustomFormatter: function ( docDefinition ) {
                  docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                  docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                  return docDefinition;
              },
              exporterPdfOrientation: 'portrait',
              exporterPdfPageSize: 'LETTER',
              exporterPdfMaxGridWidth: 500,
              exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
              onRegisterApi: function(gridApi){
                  // $scope.gridApi = gridApi;
                  pl.gridApi = gridApi;
              },
              data: [],
              rowStyle: function(row){},
              rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
              '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
              'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
              'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
              '</div>' +
              '</div>'
          };

          //data
          pl.table_data.forEach(function (res,$index) {

              var temp = {
                  full_info : res,
                  id : res.id,
                  created_at : $filter('date')(new Date(res.created_at),'yyyy-MM-dd'),
                  created_by : res.created_by,
                  grand_price : res.grand_price,
                  consigning_on : $filter('date')(new Date(res.consigning_on),'yyyy-MM-dd'),
                  number_of_consignment : res.number_of_consignment,
                  number_of_items_in_consignment :  res.number_of_items_in_consignment,
                  idle_count: res.idle_count,
                  // source_vendor : (res.source_vendor == 0 ? 0 : pl.vendor_list[res.source_vendor]),
                  destination_vendor : (res.destination_vendor == 0 ? 0 : pl.vendor_list[res.destination_vendor]),
                  comment : res.comment,
                  // is_active : res.is_active,
                  to_be_received_on : $filter('date')(new Date(res.to_be_received_on),'yyyy-MM-dd')
              };

              pl.gridOptionsComplex.data.push(temp);

          });

      }
      else{
          pl.gridOptionsComplex = {
              data : []
          };
      }
    }

    function exportToCsv() {

        pl.csv_array = [];

        pl.gridApi.grid.rows.forEach(function (row) {
            if (row.visible) {

                var temp = {
                    id : row.entity.id,
                    created_at : row.entity.created_at,
                    created_by : row.entity.created_by,
                    grand_price : row.entity.grand_price,
                    consigning_on : row.entity.consigning_on,
                    number_of_consignment : row.entity.number_of_consignment,
                    number_of_items_in_consignment : row.entity.number_of_items_in_consignment,
                    idle_count : row.entity.idle_count,
                    // source_vendor : row.entity.source_vendor,
                    destination_vendor : row.entity.destination_vendor,
                    comment : row.entity.comment,
                    // is_active : row.entity.is_active,
                    to_be_received_on : row.entity.to_be_received_on
                };

                pl.csv_array.push(temp);

            }
        });

        return pl.csv_array;


    }

    function getVendorList() {

        pl.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'vendor/',
            headers: mainVm.headers
        }).success(function (response) {

            pl.vendor_list = {};

            response.data.forEach(function (col) {
                pl.vendor_list[col.id] = col.user_name;
            });

            pl.spinners     = false;
            // pl.getData(); // Default call

        });

    }

    function viewDetails(to_details) {

        pl.to_details = to_details;

        ngDialog.open({
            template: 'viewDetails',
            className: 'ngdialog-theme-default consignment-view-modal',
            showClose: false,
            scope: $scope
        });

    }

    function confirm(pick_list_id) {

        pl.pick_list_id = pick_list_id.id;
        pl.host_user_name = pick_list_id.boutique_vendors_info.user_name;
        if(pick_list_id.trunk_show_event_id){
          pl.trunk_id = pick_list_id.trunk_show_event_id;
        }

        ngDialog.open({
            template: 'confirm',
            className: 'ngdialog-theme-default consignment-view-modal',
            showClose: false,
            scope: $scope
        });

    }

    function disablePicklist(id) {
      pl.disable = id;
      if(pl.disable == 'yes'){
        pl.spinners = true;
        $http({
            method: 'PUT',
            url: mainVm.urlList.flymall + 'catalog/pickLists-marked-deactivate/',
            data : {
                "pick_list_id" : pl.pick_list_id
            },
            headers: mainVm.headers
        }).success(function (response) {
          pl.spinners = false;
          pl.log_url();
          getTableData();
          ngDialog.close({
              template: 'confirm',
          });
        })
      }else{
        ngDialog.close({
            template: 'confirm',
        });
      }
    }

}


angular
    .module('flyrobe')
    .controller('picklistCtrl', picklistCtrl);
