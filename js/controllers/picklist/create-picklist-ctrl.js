/**
 ********** Created by Surbhi Goel ************
 * delete it
 **/

function createPicklistCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                               mainServices,ngDialog) {

    con = this;
    con.panel = "PICK";

    if(!$filter('tab')(con.panel)) {
        $state.go('login');
    }


    con.spinner = true;
    con.show_vendor_details = false;

    con.vendor_list = [];
    con.sender_list = [];
    con.selected_items = [];
    con.cons = {};
    con.selected_manufacture = "";

    mainVm.flyrobeState     = "Create T.O.";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    con.createPicklist               = createPicklist;
    con.getItemList                  = getItemList;
    con.getManufacturerList          = getManufacturerList;
    con.getVendorList                = getVendorList;
    con.openModalForImage            = openModalForImage;
    con.selectCheckBox               = selectCheckBox;
    con.getVendorDetails             = getVendorDetails;

    con.getVendorList(); //Default Call

    function createPicklist() {

        con.spinners     = true;

        var send_data = {
            "destination_vendor" : (con.cons.vendor && con.cons.vendor.id ? con.cons.vendor.id : 0 ),
            "parent_items" : [],
            "source_vendor" : (con.cons.sender && con.cons.sender.id ? con.cons.sender.id : 0 ),
            "assign_to" : (con.cons.assign_to ? con.cons.assign_to : ""),
            "comment" : (con.cons.comment ? con.cons.comment : '')
        };

        if(!con.cons.sender || !con.cons.sender.id){
            SweetAlert.swal({
                title: "",
                text: "Please select Sender."
            });
            con.spinners = false;
            return false;
        }

        if(!con.cons.vendor || !con.cons.vendor.id){
            SweetAlert.swal({
                title: "",
                text: "Please select Host."
            });
            con.spinners = false;
            return false;
        }

        if(con.selected_items.length <= 0){
            SweetAlert.swal({
                title: "",
                text: "Please select atleast one item."
            });
            con.spinners = false;
            return false;
        }

        var temp_arr = [];

        con.selected_items.forEach(function (col) {

            if(temp_arr[col.size + '&&&&' + col.style_details_id]){
                temp_arr[col.size + '&&&&' + col.style_details_id] += 1;
            }
            else{
                temp_arr[col.size + '&&&&' + col.style_details_id] = 1;
            }
            
        });

        for(var a in temp_arr){

            var val = a.split('&&&&');
            var temp = {
                "style_id" : parseInt(val[1]),
                "size" : val[0],
                "required_count" : temp_arr[a]
            };
            send_data.parent_items.push(temp);

        }

        $http({
            method: 'POST',
            url: mainVm.urlList.flymall + 'pick-list/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            con.spinners = false;

            if(response.is_error){

                if(typeof(response.message) == 'string'){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message[0]
                    });
                }

            }
            else{
                con.manufacture = {};
                SweetAlert.swal({
                    title: "",
                    text: "Created (" + response.data.id + ")"
                },function () {
                    con.selected_items = [];
                    $state.go('flyrobe.picklist');
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function getVendorList() {

        con.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'vendor/',
            headers: mainVm.headers
        }).success(function (response) {

            con.getManufacturerList();

            con.vendor_list = [];
            con.sender_list = [];

            response.data.forEach(function (col) {

                if(col.type == 'WRHS'){
                    con.sender_list.push(col);
                }
                else{
                    con.vendor_list.push(col);
                }

            });

            // con.spinners    = false;

        });

    }

    function getManufacturerList() {

        con.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'manufacturer/',
            headers: mainVm.headers
        }).success(function (response) {

            con.manufacturer_list = {};
            con.manufacturer_list_2 = response.data;
            response.data.forEach(function (col) {
                con.manufacturer_list[col.id] = col.user_name;
            });

            con.spinners     = false;

        });

    }

    function getItemList() {

        con.spinners     = true;
        // con.selected_items = [];

        var manuf_id = con.selected_manufacture;
        if(con.selected_manufacture == 'all'){
            manuf_id = "";
        }

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall2 + 'catalog/items/?manufacturer_id=' + manuf_id
            + "&status=AVL",
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            con.spinners = false;
            table_data = response.data;

            if(table_data.length){

                //column def
                {
                    var column = [];
                    con.csv_header = [];

                    var col_name = {
                        name : 'full_info',
                        displayName: '',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="checkbox c-checkbox needsclick p-8 h-100p"' +
                        'ng-click="grid.appScope.con.selectCheckBox(COL_FIELD)"> ' +
                        '<label class="needsclick"> ' +
                        '<i class="fa fa-square-o" ng-hide="row.entity.full_info.checked"></i> ' +
                        '<i class="fa fa-check-square-o" ng-show="row.entity.full_info.checked"></i> ' +
                        '</label> ' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Id');

                    var col_name = {
                        name : 'id',
                        displayName: 'Id',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Id');

                    var col_name = {
                        name: 'image',
                        displayName: 'Image',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p"> ' +
                        '<img src="{{COL_FIELD}}" alt="Image" ng-click="grid.appScope.con.openModalForImage(row.entity.image2)"' +
                        'class="img-responsive img-circle"' +
                        'style="height: 40px; width: 25px; cursor: pointer;"/> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'item_code',
                        displayName: 'Item Code',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Item Code');

                    var col_name = {
                        name : 'manufacturer',
                        displayName: 'Merchant',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Merchant');

                    var col_name = {
                        name : 'size',
                        displayName: 'Size',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Size');

                    var col_name = {
                        name : 'status',
                        displayName: 'Status',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Status');


                }

                con.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 70,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        con.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){},
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                table_data.forEach(function (res) {

                    var temp = {
                        full_info : res,
                        id : res.id,
                        item_code : res.item_code,
                        manufacturer : con.manufacturer_list[res.manufacturer_id],
                        size : res.size,
                        image : mainVm.urlList.imagekit_100_url + res.style_info.image_front,
                        image2 : mainVm.urlList.imagekit_url + res.style_info.image_front,
                        status : res.status
                    };

                    if(res.style_info.image_front.indexOf('http') >= 0){
                        temp.image = res.style_info.image_front;
                        temp.image2 = res.style_info.image_front;
                    }

                    con.gridOptionsComplex.data.push(temp);

                });

            }
            else{
                con.gridOptionsComplex = {
                    data : []
                };
            }

        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function selectCheckBox(col) {

        if(col.checked){
            col.checked = false;

            var i = con.selected_items.indexOf(col);

            if (i != -1) {
                con.selected_items.splice(i, 1);
            }

        }
        else{
            col.checked = true;

            var i = con.selected_items.indexOf(col);

            if (i == -1) {
                con.selected_items.push(col);
            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Already added"
                });
            }
        }

    }

    function openModalForImage(url) {
        con.imagePath = url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }

    function getVendorDetails(vendor) {

        con.show_vendor_details = true;
        // con.vendor_details = vendor;

    }

}


angular
    .module('flyrobe')
    .controller('createPicklistCtrl', createPicklistCtrl);