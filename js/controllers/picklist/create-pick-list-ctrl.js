/**
 ********** Created by Surbhi Goel ************
 **/

function createPicklistCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                            mainServices,ngDialog) {

    con = this;
    con.panel = "PICK";

    if(!$filter('tab')(con.panel)) {
        $state.go('login');
    }


    con.spinner = true;
    con.show_vendor_details = false;
    con.vendor_list = [];
    con.sender_list = [];
    con.remove_items = [];
    con.selected_items = [];
    con.cons = {};
    con.all = {
                first_name: 'All',
                last_name: '',
                user_name: 'All',
                id:''
              }
    con.selected_manufacture = "";
    con.added_qty = 0;

    mainVm.flyrobeState     = "Create Order";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    con.changeConfirmDate            = changeConfirmDate;
    con.createPicklist               = createPicklist;
    con.getItemList                  = getItemList;
    con.getManufacturerList          = getManufacturerList;
    con.getTagList                   = getTagList;
    con.getVendorList                = getVendorList;
    con.openModalForImage            = openModalForImage;
    con.selectCheckBox               = selectCheckBox;
    con.removeAll                    = removeAll;
    con.getVendorDetails             = getVendorDetails;
    con.check_items                  = check_items;
    con.getTrunkshowList             = getTrunkshowList;

    con.getVendorList(); //Default Call
    con.getTrunkshowList();

    function getTrunkshowList(){
      con.spinners     = true;

      $http({
          method: 'GET',
          url: mainVm.urlList.flymall2 + 'trunk-show-event/',
          headers: mainVm.headers
      }).success(function (response) {
          con.spinners     = false;
          con.trunkshow_list = response.data;
      })
    }

    function createPicklist() {

        con.spinners     = true;

        var send_data = {
            "destination_vendor" : (con.cons.vendor && con.cons.vendor.host_id ? con.cons.vendor.host_id : 0 ),
            "parent_items" : [],
            "trunk_show_event_id" : (con.cons.vendor && con.cons.vendor.id ? con.cons.vendor.id : 0 ),
            "source_vendor" : (con.cons.sender && con.cons.sender.id ? con.cons.sender.id : 0 ),
            "assign_to" : (con.cons.assign_to ? con.cons.assign_to : ""),
            "comment" : (con.cons.comment ? con.cons.comment : ''),
            "reserve_for_days" : (con.cons.days ? parseInt(con.cons.days) : ''),
            "to_be_received_on" : con.cons.date,
            "idle_count" : parseInt(con.cons.ideal_count)
            // "to_be_dispatch_at" : con.cons.date
        };

        if(!con.cons.sender || !con.cons.sender.id){
            SweetAlert.swal({
                title: "",
                text: "Please select Sender."
            });
            con.spinners = false;
            return false;
        }

        if(!con.cons.vendor || !con.cons.vendor.id){
            SweetAlert.swal({
                title: "",
                text: "Please select Host."
            });
            con.spinners = false;
            return false;
        }

        if(con.selected_items.length <= 0){
            SweetAlert.swal({
                title: "",
                text: "Please select atleast one item."
            });
            con.spinners = false;
            return false;
        }

        var temp_arr = [];

        con.selected_items.forEach(function (col) {

            var temp = {
                "style_id" : parseInt(col.id),
                "size" : col.item_info.size,
                "required_count" : col.required_items
            };
            send_data.parent_items.push(temp);

        });

        if(con.cons.new_margin_type == "PER"){
            send_data.updated_margin_rate = parseInt(con.cons.new_margin_rate);
        }
        else{
            send_data.updated_margin_rate_absolute = parseInt(con.cons.new_margin_rate_absolute);
        }

        /*con.selected_items.forEach(function (col) {

            if(temp_arr[col.size + '&&&&' + col.style_details_id]){
                temp_arr[col.size + '&&&&' + col.style_details_id] += 1;
            }
            else{
                temp_arr[col.size + '&&&&' + col.style_details_id] = 1;
            }

        });

        for(var a in temp_arr){

            var val = a.split('&&&&');
            var temp = {
                "style_id" : parseInt(val[1]),
                "size" : val[0],
                "required_count" : temp_arr[a]
            };
            send_data.parent_items.push(temp);

        }*/

        $http({
            method: 'POST',
            url: mainVm.urlList.flymall + 'pick-list/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            con.spinners = false;

            if(response.is_error){
                con.remove_items = [];
                if(typeof(response.message) == 'string'){
                  if(response.data.not_available_items.length > 0 && response.data.already_assigned.length > 0){
                      SweetAlert.swal({
                          title: "",
                          text: response.message + ' \n' + response.data.not_available_items + ' \n' + ' \n' + 'Already Assigned' + ' \n' + response.data.already_assigned,
                      });
                  }else if(response.data.not_available_items.length > 0){
                      SweetAlert.swal({
                          title: "",
                          text: response.message + ' \n' + response.data.not_available_items,
                      });
                  }else if(response.data.already_assigned.length > 0){
                    SweetAlert.swal({
                        title: "",
                        text: response.message + ' \n' + response.data.already_assigned,
                    });
                  }else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                  }
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message[0]
                    });
                }
                if(response.data.not_available_items.length > 0){
                  con.remove_items = response.data.not_available_items
                }
                else if(response.data.already_assigned.length > 0){
                  var already_assigned = response.data.already_assigned;
                  con.selected_items.map((val) => {
                    if(already_assigned.includes(val.id)){
                      var custom_id = val.identification_code + '-' + val.item_info.size;
                      con.remove_items.push(custom_id)
                    }
                  })
                }
                console.log(con.remove_items)
            }
            else{
                con.manufacture = {};
                SweetAlert.swal({
                    title: "",
                    text: "Created (" + response.data.id + ")"
                },function () {
                    con.selected_items = [];
                    $state.go('flyrobe.picklist');
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function check_items(id) {
      var custom_id = id.identification_code + '-' + id.item_info.size;
      if(con.remove_items.includes(custom_id)){
        return true;
      }else{
        return false;
      }
    }

    function getVendorList() {

        con.spinners     = true;
        con.added_qty    = 0;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'vendor/',
            headers: mainVm.headers
        }).success(function (response) {

            con.getManufacturerList();
            con.getTagList();
            con.vendor_list = [];
            con.sender_list = [];

            response.data.forEach(function (col) {

                if(col.type == 'WRHS'){
                    con.sender_list.push(col);

                    if(col.id == 22){
                        con.cons.sender = col;
                    }
                }
                else{
                    con.vendor_list.push(col);
                }

            });

            // con.spinners    = false;

        });

    }

    function getManufacturerList() {

        con.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'manufacturer/',
            headers: mainVm.headers
        }).success(function (response) {

            con.manufacturer_list = {};
            con.manufacturer_list_2 = response.data;
            con.manufacturer_list_2.push(con.all)
            response.data.forEach(function (col) {
                con.manufacturer_list[col.id] = col.user_name;
            });

            con.spinners     = false;

        });

    }

    function getTagList() {

        con.spinners = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall2 + 'catalog/tags',
            headers: mainVm.headers
        }).success(function (response) {
            con.tag_list = response.data;
            con.spinners = false;
        });

    }

    function getItemList() {

        con.spinners     = true;
        // con.selected_items = [];
        var tag_id = [];
        if(con.selected_tags){
          con.selected_tags.map((item) => {
            tag_id.push(item.id)
          })
        }

        var manuf_id = con.selected_manufacture.id;
        if(con.selected_manufacture.first_name == 'All' || manuf_id == undefined){
            manuf_id = "";
            if(tag_id.length>0 && con.cons.range){
              var url = mainVm.urlList.flymall2 + 'catalog/items/level-two/?host_id=' + con.cons.vendor.host_id + '&tags=' + tag_id + "&price=" + con.cons.range + "&status=AVL";
            }else if(tag_id.length>0){
              var url = mainVm.urlList.flymall2 + 'catalog/items/level-two/?host_id=' + con.cons.vendor.host_id + '&tags=' + tag_id + "&status=AVL";
            }else if(con.cons.range){
              var url = mainVm.urlList.flymall2 + 'catalog/items/level-two/?host_id=' + con.cons.vendor.host_id + "&price=" + con.cons.range + "&status=AVL";
            }else{
              var url = mainVm.urlList.flymall2 + 'catalog/items/level-two/?host_id=' + con.cons.vendor.host_id + '&status=AVL';
            }
        }
        else{
            if(tag_id.length>0 && con.cons.range){
              var url = mainVm.urlList.flymall2 + 'catalog/items/level-two/?host_id=' + con.cons.vendor.host_id + '&manufacturer_id=' + manuf_id + '&tags=' + tag_id + "&price=" + con.cons.range + "&status=AVL";
            }else if(tag_id.length>0){
              var url = mainVm.urlList.flymall2 + 'catalog/items/level-two/?host_id=' + con.cons.vendor.host_id + '&manufacturer_id=' + manuf_id + '&tags=' + tag_id + "&status=AVL";
            }else if(con.cons.range){
              var url = mainVm.urlList.flymall2 + 'catalog/items/level-two/?host_id=' + con.cons.vendor.host_id + '&manufacturer_id=' + manuf_id + "&price=" + con.cons.range + "&status=AVL";
            }else{
              var url = mainVm.urlList.flymall2 + 'catalog/items/level-two/?host_id=' + con.cons.vendor.host_id + '&manufacturer_id=' + manuf_id + "&status=AVL";
            }
        }

        $http({
            method: 'GET',
            url : url,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            con.spinners = false;
            con.table_data = response.data;

            if(con.table_data.length){

                //column def
                {
                    var column = [];
                    con.csv_header = [];

                    var col_name = {
                        name : 'full_info',
                        displayName: '',
                        width : 70,
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="checkbox c-checkbox needsclick p-8 h-100p"' +
                        'ng-click="grid.appScope.con.selectCheckBox(COL_FIELD)"> ' +
                        '<label class="needsclick"> ' +
                        '<i class="fa fa-square-o" ng-hide="row.entity.full_info.checked"></i> ' +
                        '<i class="fa fa-check-square-o" ng-show="row.entity.full_info.checked"></i> ' +
                        '</label> {{row.entity.full_info.id}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Id');

                    var col_name = {
                        name: 'image',
                        displayName: 'Image',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p"> ' +
                        '<img src="{{COL_FIELD}}" alt="Image" ng-click="grid.appScope.con.openModalForImage(row.entity.image2)"' +
                        'class="img-responsive img-circle"' +
                        'style="height: 40px; width: 25px; cursor: pointer;"/> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'available_items',
                        displayName: 'Available',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Available');

                    var col_name = {
                        name : 'already_assigned_to_picklist',
                        displayName: 'Assigned',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Assigned To Picklist');

                    var col_name = {
                        name : 'identification_code',
                        displayName: 'identification Code',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('identification Code');

                    var col_name = {
                        name : 'manufacturer',
                        displayName: 'Merchant',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Merchant');

                    var col_name = {
                        name : 'size',
                        displayName: 'Size',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Size');

                    var col_name = {
                        name : 'category',
                        displayName: 'Category',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Category');

                    var col_name = {
                        name : 'cost',
                        displayName: 'Cost',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Cost');

                    var col_name = {
                        name : 'price',
                        displayName: 'Price',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Price');

                    var col_name = {
                        name : 'short_description',
                        displayName: 'Short Description',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Short Description');

                    var col_name = {
                        name : 'is_already_sent',
                        displayName: 'Is Sent Before',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Is Sent Before');


                }

                con.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 70,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        con.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){},
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                con.table_data.forEach(function (res) {

                    var temp = {
                        full_info : res,
                        id : res.id,
                        available_items : res.available_items,
                        identification_code : res.identification_code,
                        manufacturer : res.manufacturers_info.first_name + " " + res.manufacturers_info.last_name,
                        size : res.item_info.size,
                        already_assigned_to_picklist : res.already_assigned_to_picklist,
                        image : mainVm.urlList.imagekit_100_url + res.image_front,
                        image2 : mainVm.urlList.imagekit_url + res.image_front,
                        category : res.category,
                        cost : res.cost,
                        price : res.price,
                        short_description : res.short_description,
                        is_already_sent: res.is_already_sent
                    };

                    if(res.image_front.indexOf('http') >= 0){
                        temp.image = res.image_front;
                        temp.image2 = res.image_front;
                    }

                    con.gridOptionsComplex.data.push(temp);

                });

            }
            else{
                con.gridOptionsComplex = {
                    data : []
                };
            }

        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function removeAll(){

      con.removevableItems = [];

      con.selected_items.map((val) => {
        if(con.remove_items.includes(val.identification_code+'-'+val.item_info.size)){
          con.removevableItems.push(val);
        }
      })

      con.removevableItems.map((item) => {
        con.selectCheckBox(item);
      })

      con.remove_items = [];
    }

    function selectCheckBox(col) {
        if(col.checked){
            col.checked = false;

            var i = con.selected_items.indexOf(col);

            if (i != -1) {
                con.added_qty = con.added_qty - col.required_items;
                con.selected_items.splice(i, 1);
            }

        }
        else{
            col.checked = true;

            var i = con.selected_items.indexOf(col);

            if (i == -1) {
                col.required_items = 1;
                con.added_qty += 1;
                con.selected_items.push(col);
            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Already added"
                });
            }
        }

        var objDiv = document.getElementById("seleted-items-div");

        $('#seleted-items-div').animate({
            scrollTop: objDiv.scrollHeight
        }, 500);

    }

    function openModalForImage(url) {
        con.imagePath = url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }

    function date_diff_indays (date1, date2) {
      dt1 = new Date(date1);
      dt2 = new Date(date2);
      return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }

    function getVendorDetails(val) {

        con.selected_manufacture = con.all;
        con.selected_tags = null;
        con.cons.date = $filter('date')(new Date(val.event_start_date),'yyyy-MM-dd');
        con.cons.end_date = $filter('date')(new Date(val.event_end_date),'yyyy-MM-dd');
        con.cons.ideal_count = val.required_count;
        if(con.cons.date && con.cons.end_date){
          con.cons.days = date_diff_indays(con.cons.date, con.cons.end_date) + 1;
        }

        // con.getItemList();

        console.log(val)

        var vendor = '';

        con.vendor_list.map((item) => {
          if(item.id == val.host_id){
            vendor = item;
          }
        })

        if(vendor.margin_rate > 0 ){
            con.cons.new_margin_type = "PER";
            con.cons.new_margin_rate = vendor.margin_rate;
            con.cons.new_margin_rate_absolute = 0;
        }
        else{
            con.cons.new_margin_type = "ABS";
            con.cons.new_margin_rate = 0;
            con.cons.new_margin_rate_absolute = vendor.margin_rate_absolute;
        }

        con.show_vendor_details = true;
        // con.vendor_details = vendor;

    }

    function changeConfirmDate() {
        con.cons.days = '';
        $('.confirm-date-div input').removeClass('parsley-error');
        $('.confirm-date-div ul').css('display','none');
    }


}


angular
    .module('flyrobe')
    .controller('createPicklistCtrl', createPicklistCtrl);
