

function trunkshowCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                         mainServices,ngDialog,DTOptionsBuilder) {

    ts = this;
    ts.panel = "TRUNK";

    // if(!$filter('tab')(ts.panel)) {
    //     $state.go('login');
    // }

    ts.cons = {};
    mainVm.flyrobeState     = "Trunkshow";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    ts.getData                          = getData;
    ts.exportToCsv                      = exportToCsv;
    ts.editDetails                      = editDetails;
    ts.changeDate                       = changeDate;
    ts.changeDateStart                  = changeDateStart;
    ts.updateTrunkshow                  = updateTrunkshow;

    ts.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withDisplayLength(50)
        .withOption('order', [1, 'asc'])
        .withButtons([
            {extend: 'copy'}
        ]);// dataTable Searching , Sorting & pagination

    function date_diff_indays (date1, date2) {
      dt1 = new Date(date1);
      dt2 = new Date(date2);
      return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }

    function changeDateStart () {
      const new_date = new Date(ts.cons.start_date)
      new_date.setDate(new_date.getDate() - 1);
      ts.cons.start_enddate = $filter('date')(new Date(new_date), 'yyyy-MM-dd');
      ts.cons.end_date = '';
      ts.cons.total_days = null;
    }

    function changeDate() {
      if(ts.cons.start_date && ts.cons.end_date){
        ts.cons.total_days = date_diff_indays(ts.cons.start_date, ts.cons.end_date) + 1;
      }
    }

    function getData() {

        console.log("ts.date_range ", $('#rangeInput').val());

        if($('#rangeInput').val() == "" || $('#rangeInput').val() == undefined || $('#rangeInput').val() == null){
            var url = mainVm.urlList.flymall2 + 'trunk-show-event/';
        }
        else{
            var date = $('#rangeInput').val().split(' ');
            var url = mainVm.urlList.flymall2 + 'trunk-show-event/?event_start_date=' + date[0] + '&event_end_date=' + date[2];
        }

        ts.spinners     = true;

        $http({
            method: 'GET',
            url: url,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            ts.spinners = false;
            ts.table_data = response.data;

            getTableData();

        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function getTableData() {

      if(ts.table_data.length){

          //column def
          {
              var column = [];
              ts.csv_header = [];

              var col_name = {
                  name : 'id',
                  displayName: 'Id',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p">' +
                  '{{COL_FIELD}}<br>' +
                  '</a>' +
                  '</div>'
              };
              column.push(col_name);
              ts.csv_header.push('Id');

              var col_name = {
                  name : 'status',
                  displayName: 'Status',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              ts.csv_header.push('Status');

              var col_name = {
                  name : 'host_name',
                  displayName: 'Host Name',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              ts.csv_header.push('Host Name');

              var col_name = {
                  name : 'host_phonenumber',
                  displayName: 'Phone Number',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              ts.csv_header.push('Phone Number');

              var col_name = {
                  name : 'event_start_date',
                  displayName: 'Start Date',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              ts.csv_header.push('Start Date');

              var col_name = {
                  name : 'event_end_date',
                  displayName: 'End Date',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              ts.csv_header.push('End Date');

              var col_name = {
                  name : 'to_be_received_on',
                  displayName: 'To Be Received Date',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              ts.csv_header.push('To Be Received Date');

              var col_name = {
                  name : 'required_count',
                  displayName: 'Total Items Requested',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              ts.csv_header.push('Total Items Requested');

              var col_name = {
                  name : 'created_at',
                  displayName: 'Order Creation Date',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              ts.csv_header.push('Order Creation Date');

              var col_name = {
                  name : 'pin_code',
                  displayName: 'Pincode',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              ts.csv_header.push('Pincode');

              var col_name = {
                  name : 'address',
                  displayName: 'Address',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '{{COL_FIELD}}' +
                  '</div>'
              };
              column.push(col_name);
              ts.csv_header.push('Address');

              var col_name = {
                  name : 'edit_trunkshow',
                  displayName: 'Edit Trunkshow',
                  enableCellEdit: false,
                  cellTemplate : '' +
                  '<div class="p-8 h-100p word-break-all">' +
                  '<a ng-click="grid.appScope.ts.editDetails(row.entity.full_info)"' + 'ng-if="row.entity.full_info.status == \'CRTD\'">' +
                  'Edit' +
                  '</a>' +
                  '</div>'
              };
              column.push(col_name);
          }

          ts.gridOptionsComplex = {
              enableFiltering: true,
              showGridFooter: true,
              showColumnFooter: true,
              paginationPageSizes: [10, 25, 50,100],
              paginationPageSize: 100,
              rowHeight: 100,
              columnDefs: column,
              exporterCsvFilename: 'myFile.csv',
              exporterPdfDefaultStyle: {fontSize: 9},
              exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
              exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
              exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
              exporterPdfFooter: function ( currentPage, pageCount ) {
                  return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
              },
              exporterPdfCustomFormatter: function ( docDefinition ) {
                  docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                  docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                  return docDefinition;
              },
              exporterPdfOrientation: 'portrait',
              exporterPdfPageSize: 'LETTER',
              exporterPdfMaxGridWidth: 500,
              exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
              onRegisterApi: function(gridApi){
                  // $scope.gridApi = gridApi;
                  ts.gridApi = gridApi;
              },
              data: [],
              rowStyle: function(row){},
              rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
              '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
              'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
              'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
              '</div>' +
              '</div>'
          };

          //data
          ts.table_data.forEach(function (res,$index) {

              var temp = {
                  full_info : res,
                  id : res.id,
                  status: res.status,
                  host_name: res.boutique_vendors_first_name + ' ' + res.boutique_vendors_last_name,
                  host_phonenumber: res.boutique_vendors_phone_number,
                  event_start_date: $filter('date')(new Date(res.event_start_date),'yyyy-MM-dd'),
                  event_end_date: $filter('date')(new Date(res.event_end_date),'yyyy-MM-dd'),
                  to_be_received_on: $filter('date')(new Date(res.to_be_received_on),'yyyy-MM-dd'),
                  required_count: res.required_count,
                  created_at: $filter('date')(new Date(res.created_at),'yyyy-MM-dd'),
                  pin_code: res.pin_code,
                  address: res.address + ', ' + res.city
              };

              ts.gridOptionsComplex.data.push(temp);

          });

      }
      else{
          ts.gridOptionsComplex = {
              data : []
          };
      }
    }

    function editDetails(to_details) {

        ts.to_details = to_details;
        ts.cons.start_date = $filter('date')(new Date(ts.to_details.event_start_date),'yyyy-MM-dd');
        ts.cons.end_date = $filter('date')(new Date(ts.to_details.event_end_date),'yyyy-MM-dd');
        ts.cons.address = ts.to_details.address;
        ts.cons.id = ts.to_details.id;

        ngDialog.open({
            template: 'editDetails',
            className: 'ngdialog-theme-default consignment-view-modal',
            showClose: false,
            scope: $scope
        });

    }

    function exportToCsv() {

        ts.csv_array = [];

        ts.gridApi.grid.rows.forEach(function (row) {
            if (row.visible) {

                var temp = {
                    id : row.entity.id,
                    status : row.entity.status,
                    host_name : row.entity.host_name,
                    host_phonenumber : row.entity.host_phonenumber,
                    event_start_date : row.entity.event_start_date,
                    event_end_date : row.entity.event_end_date,
                    to_be_received_on : row.entity.to_be_received_on,
                    required_count : row.entity.required_count,
                    created_at : row.entity.created_at,
                    pin_code : row.entity.pin_code,
                    address : row.entity.address,
                };

                ts.csv_array.push(temp);

            }
        });

        return ts.csv_array;


    }

    function updateTrunkshow() {

      if(ts.cons.end_date == ''){
          SweetAlert.swal({
              title: "",
              text: "Please select End Date."
          });
          con.spinners = false;
          return false;
      }

        ngDialog.close({
            template: 'editDetails',
            className: 'ngdialog-theme-default',
            showClose: true,
            scope: $scope
        });

        var send_data = {
          "id": ts.cons.id,
          "is_create_by_us":true,
          "event_start_date": ts.cons.start_date,
          "event_end_date": ts.cons.end_date,
          "address": (ts.cons.address ? ts.cons.address : ''),
        }

        $http({
            method: 'PUT',
            url: mainVm.urlList.flymall2 + 'trunk-show-event/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {
          ts.getData()
          console.log("updated")
        })

    }

}

angular
    .module('flyrobe')
    .controller('trunkshowCtrl', trunkshowCtrl);
