
function createTrunkshowCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                            mainServices,ngDialog) {

    con = this;
    con.panel = "TRUNK";

    // if(!$filter('tab')(con.panel)) {
    //     $state.go('login');
    // }


    con.spinner = true;
    con.show_vendor_details = false;

    con.vendor_list = [];
    con.tag_id = [];
    con.size_list = [];
    con.category_preference = [];
    con.cons = {};
    con.final_count = 0;
    con.boxtype = [{'id':'STDN', 'boxtype_name': 'Standard'}, {'id':'CSTM', 'boxtype_name': 'Custom'}];
    con.sizedetails = ['32','34','36','38','40','42','44','46','48','FS'];
    con.category = ['Stiched','Semi-Stiched','Unstiched'];

    mainVm.flyrobeState     = "Create Trunkshow";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    con.createTrunkshow              = createTrunkshow;
    con.getVendorDetails             = getVendorDetails;
    con.changeDate                   = changeDate;
    con.changeDateStart              = changeDateStart;
    con.getVendorList                = getVendorList;
    con.getBoxType                   = getBoxType;
    con.getBoxRequirement            = getBoxRequirement;
    con.getBoxRequirementCustom      = getBoxRequirementCustom;
    con.checkBoxType                 = checkBoxType;
    con.getItemCount                 = getItemCount;
    con.getSizeList                  = getSizeList;
    con.getCategoryPreference        = getCategoryPreference;

    con.getVendorList(); //Default Call

    function getItemCount(){
        var counter = [];
        con.final_count = 0;

        angular.forEach(con.cons.items_count_arr, function(value, key) {
            if(isNaN(parseInt(value))){
              counter.push(0);
            }else{
              counter.push(parseInt(value));
            }
        });
        if(counter.length>0){
          counter.map(item => {
            con.final_count += item;
          })
        }
    }

    function getSizeList() {
      con.size_list = [];
      if(con.cons.sizedetails){
        con.cons.sizedetails.map((item) => {
          con.size_list.push(item)
        })
      }
    }

    function getCategoryPreference() {
      con.category_preference = [];
      if(con.cons.category_preference){
        con.cons.category_preference.map((item) => {
          con.category_preference.push(item)
        })
      }
    }

    function getBoxRequirement() {
      con.tag_id = [];
      con.id = [];
      if(con.cons.boxdetails){
          con.tag_id.push(con.cons.boxdetails)
          con.id.push(con.cons.boxdetails.id)
      }
      con.checkBoxType();
    }

    function getBoxRequirementCustom() {
      con.tag_id = [];
      con.id = [];
      if(con.cons.boxdetails){
          con.cons.boxdetails.map((item) => {
            con.tag_id.push(item)
            con.id.push(item.id)
          })
      }
      con.checkBoxType();
    }

    function checkBoxType() {
      if(con.cons.items_count_arr){
        angular.forEach(con.cons.items_count_arr, function(value, key) {
            if(!(con.id.includes(parseInt(key)))){
              delete con.cons.items_count_arr[key];
            }
        });
      }
      con.getItemCount();
    }

    function getBoxType() {
      con.spinners = true;
      con.boxdetails = null;
      con.cons.boxdetails = null;
      con.cons.items_count = null;
      con.tag_id = [];
      con.id = [];
      con.category_preference = [];

      $http({
          method: 'GET',
          url: mainVm.urlList.flymall2 + 'kit-and-box-managing/kit?type=' + con.cons.boxtype.id,
          headers: mainVm.headers
      }).success(function (response) {
          con.spinners   = false;
          con.boxdetails = response.data;
      });
    }

    function getVendorList() {

        con.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'vendor/',
            headers: mainVm.headers
        }).success(function (response) {

            con.vendor_list = [];

            response.data.forEach(function (col) {

                if(col.type !== 'WRHS'){
                    con.vendor_list.push(col);
                }

            });

            con.spinners    = false;

        });

    }

    const new_date_week = new Date()
    new_date_week.setDate(new_date_week.getDate() - 7);
    con.cons.start_date_week = $filter('date')(new Date(new_date_week), 'yyyy-MM-dd');

    function date_diff_indays (date1, date2) {
      dt1 = new Date(date1);
      dt2 = new Date(date2);
      return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }

    function changeDateStart () {
      const new_date = new Date(con.cons.start_date)
      new_date.setDate(new_date.getDate() - 1);
      con.cons.start_enddate = $filter('date')(new Date(new_date), 'yyyy-MM-dd');
      con.cons.end_date = '';
      con.cons.total_days = null;
    }

    function changeDate() {
      if(con.cons.start_date && con.cons.end_date){
        con.cons.total_days = date_diff_indays(con.cons.start_date, con.cons.end_date) + 1;
      }
    }

    function createTrunkshow() {

        con.spinners     = true;

        console.log(con.cons.boxtype)
        console.log(con.tag_id.length)
        if(!con.cons.vendor || !con.cons.vendor.id){
            SweetAlert.swal({
                title: "",
                text: "Please select Host."
            });
            con.spinners = false;
            return false;
        }

        if(!con.cons.boxtype || !con.cons.boxtype.id){
            SweetAlert.swal({
                title: "",
                text: "Please select Box Type."
            });
            con.spinners = false;
            return false;
        }

        if(con.tag_id.length == 0){
            SweetAlert.swal({
                title: "",
                text: "Please select Box Requirement."
            });
            con.spinners = false;
            return false;
        }

        if(con.size_list.length == 0){
            SweetAlert.swal({
                title: "",
                text: "Please select Size Preference."
            });
            con.spinners = false;
            return false;
        }

        if(con.cons.boxtype.id=='STDN' && con.category_preference.length == 0){
            SweetAlert.swal({
                title: "",
                text: "Please Select Category Preference."
            });
            con.spinners = false;
            return false;
        }

        console.log(con.cons)

        if(con.final_count > 150){
          SweetAlert.swal({
              title: "",
              text: "Total no of items cannot Be more than 150."
          });
          con.spinners = false;
          return false;
        }

        con.box_data = [];

        angular.forEach(con.cons.items_count_arr, function(value, key) {
            con.box_data.push({
              "box_id": parseInt(key),
              "specification": parseInt(value)
            })
        });

        if(con.cons.vendor.margin_rate > 0){
          con.margin_percent = true;
          con.margin_rate = con.cons.vendor.margin_rate;
        }else{
          con.margin_percent = false;
          con.margin_rate = con.cons.vendor.margin_rate_absolute;
        }

        var send_data = {
            "is_create_by_us":true,
          	"host_id": (con.cons.vendor && con.cons.vendor.id ? con.cons.vendor.id : 0 ),
            "box_info": con.box_data,
            "category_preference" : (con.cons.category_preference ? con.cons.category_preference : []),
          	"size_preference": (con.cons.sizedetails ? con.cons.sizedetails : []),
          	"reserve_for_days": (con.cons.total_days ?  con.cons.total_days : 0),
          	"event_start_date": con.cons.start_date,
          	"event_end_date": con.cons.end_date,
            "to_be_dispatch_at": con.cons.start_date,
            "is_margin_percent": con.margin_percent,
          	"margin_rate": con.margin_rate,
          	"required_count": (con.final_count ? con.final_count : 0),
          	"address": (con.cons.vendor.address ? con.cons.vendor.address : ''),
          	"city": (con.cons.vendor.city ? con.cons.vendor.city : ''),
          	"pin_code": (con.cons.vendor.pin_code ? con.cons.vendor.pin_code : ''),
          	"comment": (con.cons.comment ? con.cons.comment : ''),
          	"sd_payment_id": con.cons.sd_payment_id
        };

        $http({
            method: 'POST',
            url: mainVm.urlList.flymall2 + 'trunk-show-event/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            con.spinners = false;

            if(response.is_error){

                if(typeof(response.message) == 'string'){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message[0]
                    });
                }

            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Created (" + response.data.id + ")"
                },function () {
                    $state.go('flyrobe.trunkshow');
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function getVendorDetails(vendor) {

        con.show_vendor_details = true;

    }

}


angular
    .module('flyrobe')
    .controller('createTrunkshowCtrl', createTrunkshowCtrl);
