/**
 ********** Created by Surbhi Goel ************
 **/

function createVendorCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                           mainServices) {

    cvCtrl = this;
    cvCtrl.panel = "SEL";

    if(!$filter('tab')(cvCtrl.panel)) {
        $state.go('login');
    }


    cvCtrl.spinner = false;
    cvCtrl.depotList = [];


    cvCtrl.type_list = [
        { 'id' : 'BUTQ', 'name' : 'Boutique'},
        { 'id' : 'TRNKS', 'name' : 'Trunk Shows'},
        // { 'id' : 'WRHS', 'name' : 'Warehouse'},
        { 'id' : 'FSTR', 'name' : 'Flyrobe Store'}
    ];

    cvCtrl.point_of_contact_list = [
        { 'id' : 'heli.mehta@flyrobe.com' , 'name' : 'Heli Mehta'},
        { 'id' : 'harshita.khandelwal@flyrobe.com' , 'name' : 'Harshita Khandelwal'},
        { 'id' : 'aishwarya.singh@flyrobe.com' , 'name' : 'Aishwarya Singh'},
        { 'id' : 'arpitta.jerath@flyrobe.com' , 'name' : 'Arpitta Jerath'},
        { 'id' : 'jitesh.phulwani@flyrobe.com' , 'name' : 'Jitesh Phulwani'},
        { 'id' : 'arko.banerjee@flyrobe.com' , 'name' : 'Arko Banerjee'},
        { 'id' : 'anshuman.sharma@flyrobe.com' , 'name' : 'Anshuman Sharma'},
        { 'id' : 'niharika.dahake@flyrobe.com' , 'name' : 'Niharika Dahake'},
        { 'id' : 'agranshu.khanna@flyrobe.com' , 'name' : 'Agranshu Khanna'},
        { 'id' : 'salim.vasaya@flyrobe.com' , 'name' : 'Salim Vasaya'}
    ];


    mainVm.flyrobeState     = "Create Host";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    cvCtrl.changeVendorType             = changeVendorType;
    cvCtrl.createVendor                 = createVendor;
    cvCtrl.changeConfirmDate            = changeConfirmDate;
    cvCtrl.getDepotList                 = getDepotList;
    cvCtrl.uploadFile                   = uploadFile;
    cvCtrl.uploadFileOnServer           = uploadFileOnServer;

    function createVendor() {

        cvCtrl.spinner     = true;

        var send_data = {
            "first_name" : cvCtrl.vendor.first_name,
            "last_name" : (cvCtrl.vendor.last_name ? cvCtrl.vendor.last_name : ''),
            "user_name" : cvCtrl.vendor.user_name,
            "email" : (cvCtrl.vendor.email ? cvCtrl.vendor.email : ''),
            "phone_number" : (cvCtrl.vendor.phone_no ? cvCtrl.vendor.phone_no : ''),
            "address" : cvCtrl.vendor.address,
            "city" : cvCtrl.vendor.city,
            "pin_code" : cvCtrl.vendor.pin_code,
            "pan_number" : cvCtrl.vendor.pan_no,
            "state" : (cvCtrl.vendor.state ? cvCtrl.vendor.state : ''),
            "country" : (cvCtrl.vendor.country ? cvCtrl.vendor.country : ''),
            "adhar_number" : (cvCtrl.vendor.aadhar_no ? cvCtrl.vendor.aadhar_no : ''),
            "stock_specification" : (cvCtrl.vendor.stock_speci ? cvCtrl.vendor.stock_speci : ''),
            "confirmed_date" : $filter('date')(new Date(cvCtrl.vendor.confirmed_date),'yyyy-MM-dd'),
            "gst_details" : (cvCtrl.vendor.gst ? cvCtrl.vendor.gst : ''),
            "id_proof" : (cvCtrl.id_proof ? cvCtrl.id_proof : ''),
            "point_of_contact" : cvCtrl.vendor.point_of_contact,
            "type" : cvCtrl.vendor.type
        };

        send_data.margin_rate = 0;

        if(cvCtrl.show_depot){
            send_data.depot_id = parseInt(cvCtrl.vendor.depot);
        }

        $http({
            method: 'POST',
            url: mainVm.urlList.flymall + 'vendor/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            cvCtrl.spinner = false;

            if(response.is_error){

                var error_msg = "";
                if(typeof(response.message) == 'string'){
                    error_msg = response.message;
                }
                else{
                    error_msg = response.message[0].message;
                }


                SweetAlert.swal({
                    title: "",
                    text: error_msg
                });
            }
            else{
                cvCtrl.manufacture = {};
                SweetAlert.swal({
                    title: "",
                    text: "Created"
                },function () {
                    $state.go('flyrobe.vendor');
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function uploadFile(input) {

        /*if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(50)
                    .height(34);
            };

            cvCtrl.id_proof = input.files[0];
            reader.readAsDataURL(input.files[0]);

            $('.image-div input').removeClass('parsley-error');
            $('.image-div ul').css('display','none');
        }*/

        cvCtrl.id_proof = input.files;

        $('.image-div input').removeClass('parsley-error');
        $('.image-div ul').css('display','none');

    }

    function changeConfirmDate() {
        $('.confirm-date-div input').removeClass('parsley-error');
        $('.confirm-date-div ul').css('display','none');
    }

    function uploadFileOnServer() {

        cvCtrl.spinner = true;

        if(cvCtrl.id_proof == "" ||cvCtrl.id_proof == null || cvCtrl.id_proof == undefined || cvCtrl.id_proof == {}){
            //no id-proof
            cvCtrl.createVendor();
        }
        else if(typeof(cvCtrl.id_proof[0]) == 'string' && cvCtrl.id_proof[0].indexOf('http') >= 0){
            //Already Uploaded on server or is empty
            cvCtrl.createVendor();
        }
        else{
            var id_proof = [];
            var len = cvCtrl.id_proof.length;

            for(var i = 0 ; i < cvCtrl.id_proof.length; i++){


                var formData = new FormData();
                formData.append('id_proof', cvCtrl.id_proof[i]);

                $.ajax({
                    type: 'POST',
                    url: mainVm.urlList.order_url + 'file-upload-to-s3',
                    dataType: "json",
                    data: formData,
                    async: true,
                    processData: false,
                    contentType: false,
                    success: function (data) {

                        id_proof.push(data.data);
                        if(id_proof.length == cvCtrl.id_proof.length){
                            cvCtrl.id_proof = id_proof;
                            cvCtrl.createVendor();
                            $scope.$apply();

                        }

                    },
                    error: function (data) {
                        mainServices.checkErrorStatus(data.status);
                    }
                });

            }




            /*var formData = new FormData();
            formData.append('id_proof', cvCtrl.id_proof);

            $.ajax({
                type: 'POST',
                url: mainVm.urlList.order_url + 'file-upload-to-s3',
                dataType: "json",
                data: formData,
                async: true,
                processData: false,
                contentType: false,
                success: function (data) {
                    cvCtrl.id_proof = data.data;
                    // cvCtrl.spinner = false;
                    cvCtrl.createVendor();
                    $scope.$apply();

                },
                error: function (data) {
                    mainServices.checkErrorStatus(data.status);
                }
            });*/

        }

    }

    function changeVendorType(type) {

        if(type == 'WRHS' || type == 'FSTR'){
            cvCtrl.show_depot = true;
            if(cvCtrl.depotList.length <= 0){
                cvCtrl.getDepotList();
            }
        }
        else{
            cvCtrl.show_depot = false;
        }

    }

    function getDepotList(){

        cvCtrl.spinner = true;

        $http({
            method: 'GET',
            url: mainVm.api  + 'depots/',
            headers: mainVm.headers
        }).success(function (data) {

            cvCtrl.spinner = false;
            cvCtrl.depotList = [];

            data.data.forEach(function (col) {
                if(col.category != 'MCT' && col.category != 'PTS'){
                    cvCtrl.depotList.push(col);
                }
            });

        });

    }

}


angular
    .module('flyrobe')
    .controller('createVendorCtrl', createVendorCtrl);
