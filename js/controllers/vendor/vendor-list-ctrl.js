/**
 ********** Created by Surbhi Goel ************
 **/

function vendorCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                         mainServices,ngDialog) {

    vc = this;
    vc.panel = "SEL";

    if(!$filter('tab')(vc.panel)) {
        $state.go('login');
    }


    mainVm.flyrobeState     = "Host";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;


    vc.type_list = {
        'BUTQ' : 'Boutique',
        'TRNKS' : 'Trunk Show',
        'WRHS' : 'Warehouse',
        'FSTR' : 'Flyrobe Store'
    };

    vc.type_list2 = [
        { 'id' : 'BUTQ' , 'name' : 'Boutique'},
        { 'id' : 'TRNKS' , 'name' : 'Trunk Show'}
    ];

    vc.point_of_contact_list = [
        { 'id' : 'heli.mehta@flyrobe.com' , 'name' : 'Heli Mehta'},
        { 'id' : 'harshita.khandelwal@flyrobe.com' , 'name' : 'Harshita Khandelwal'},
        { 'id' : 'aishwarya.singh@flyrobe.com' , 'name' : 'Aishwarya Singh'},
        { 'id' : 'arpitta.jerath@flyrobe.com' , 'name' : 'Arpitta Jerath'},
        { 'id' : 'jitesh.phulwani@flyrobe.com' , 'name' : 'Jitesh Phulwani'},
        { 'id' : 'arko.banerjee@flyrobe.com' , 'name' : 'Arko Banerjee'},
        { 'id' : 'anshuman.sharma@flyrobe.com' , 'name' : 'Anshuman Sharma'},
        { 'id' : 'niharika.dahake@flyrobe.com' , 'name' : 'Niharika Dahake'},
        { 'id' : 'agranshu.khanna@flyrobe.com' , 'name' : 'Agranshu Khanna'},
        { 'id' : 'salim.vasaya@flyrobe.com' , 'name' : 'Salim Vasaya'}
    ];
    vc.internal_vendors = [];

    vc.selected_vendor = {};


    vc.changeDate                       = changeDate;
    vc.createReturnConsignment          = createReturnConsignment;
    vc.exportToCsv                      = exportToCsv;
    vc.getData                          = getData;
    vc.openModalUpdateModal             = openModalUpdateModal;
    vc.openReturnConsignmentModal       = openReturnConsignmentModal;
    vc.openUpdateDetailsModal           = openUpdateDetailsModal;
    vc.updateMarginDetails              = updateMarginDetails;
    vc.UpdateSellerDetails              = UpdateSellerDetails;

    vc.getData(); // Default call

    function getData() {

        vc.spinners     = true;

        if(vc.point_of_contact){
            var url = mainVm.urlList.flymall + 'vendor/?type=BUTQ,TRNKS,FSTR&poc=' + vc.point_of_contact;
        }
        else{
            var url = mainVm.urlList.flymall + 'vendor/?type=BUTQ,TRNKS,FSTR';
        }

        $http({
            method: 'GET',
            url: url,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            vc.spinners = false;
            table_data = response.data;

            if(table_data.length){

                //column def
                {
                    var column = [];
                    vc.csv_header = [];

                    var col_name = {
                        name : 'checkbox',
                        displayName: '',
                        width : 40,
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ' +
                        'ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details;grid.appScope.vc.selected_index = row.entity.index">' +
                        '<label class="needsclick"> ' +
                        '<i class="fa fa-circle-o" ng-hide="grid.appScope.vc.selected_vendor.id == row.entity.full_details.id"></i> ' +
                        '<i class="fa fa-check-circle-o" ng-show="grid.appScope.vc.selected_vendor.id == row.entity.full_details.id"></i> ' +
                        '</label> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'id',
                        displayName: 'ID',
                        enableCellEdit: false,
                        width : 70,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ' +
                        'ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('ID');

                    var col_name = {
                        name : 'created_at',
                        displayName: 'Created At',
                        enableCellEdit: false,
                        width : 80,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ' +
                        'ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('Created At');

                    var col_name = {
                        name : 'user_name',
                        displayName: 'User Name',
                        enableCellEdit: false,
                        width : 100,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('User Name');

                    var col_name = {
                        name : 'name',
                        displayName: 'Host',
                        enableCellEdit: false,
                        width : 100,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('Host');

                    var col_name = {
                        name : 'type',
                        displayName: 'Type',
                        enableCellEdit: false,
                        width : 80,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('Type');

                    var col_name = {
                        name : 'margin',
                        displayName: 'Margin',
                        enableCellEdit: false,
                        width : 100,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('Margin');

                    var col_name = {
                        name : 'pan_number',
                        displayName: 'PAN',
                        enableCellEdit: false,
                        width : 100,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('PAN');

                    var col_name = {
                        name : 'adhar_number',
                        displayName: 'Aadhar',
                        enableCellEdit: false,
                        width : 100,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('Aadhar');

                    var col_name = {
                        name : 'gst_details',
                        displayName: 'GST',
                        enableCellEdit: false,
                        width : 100,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('GST');

                    var col_name = {
                        name : 'phone_number',
                        displayName: 'Phone Number',
                        width : 100,
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('Phone Number');

                    var col_name = {
                        name : 'point_of_contact',
                        displayName: 'POC',
                        width : 100,
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('POC');

                    var col_name = {
                        name : 'address',
                        displayName: 'Address',
                        enableCellEdit: false,
                        width : 250,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ng-click="grid.appScope.vc.selected_vendor = row.entity.full_details">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('Address');

                    var col_name = {
                        name : 'stock',
                        displayName: 'Stock Items',
                        width : 80,
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all">' +
                        '{{COL_FIELD}} ' +
                        '<a ng-click="grid.appScope.vc.openReturnConsignmentModal(row.entity.full_details)" ' +
                        'ng-if="row.entity.stock > 0">' +
                        ' Return' +
                        '</a>' +
                        '</div>'
                    };
                    column.push(col_name);
                    vc.csv_header.push('Stock Items');

                }

                vc.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 70,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        vc.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){

                        if(row.entity.full_details.id == vc.selected_vendor.id){
                            return 'ui-grid-active-row';
                        }

                    },
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                table_data.forEach(function (res,$index) {

                    var temp = {
                        full_details : res,
                        index : $index,
                        id : res.id,
                        created_at : $filter('date')(new Date(res.created_at),'yyyy-MM-dd'),
                        user_name : res.user_name,
                        name : res.first_name + ' ' + res.last_name,
                        type : vc.type_list[res.type],
                        pan_number : res.pan_number,
                        adhar_number : res.adhar_number,
                        gst_details : res.gst_details,
                        phone_number : res.phone_number,
                        point_of_contact : res.point_of_contact,
                        address : res.address,
                        stock : (res.stock_items ? res.stock_items.length : 0)
                    };

                    if(temp.address.indexOf(res.pin_code) < 0){
                        temp.address = res.address + " " + res.pin_code;
                    }

                    if(res.margin_rate <= 0){
                        if(res.margin_rate_absolute >= 0){
                            temp.margin = res.margin_rate_absolute;
                        }
                        else{
                            temp.margin = 0;
                        }
                    }
                    else{
                        temp.margin = res.margin_rate + " (%)"
                    }

                    vc.gridOptionsComplex.data.push(temp);

                });

            }
            else{
                vc.gridOptionsComplex = {
                    data : []
                };
            }

        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function exportToCsv() {

        vc.csv_array = [];

        vc.gridApi.grid.rows.forEach(function (row) {
            if (row.visible) {

                var temp = {
                    id : row.entity.id,
                    created_at : row.entity.created_at,
                    user_name : row.entity.user_name,
                    name : row.entity.name,
                    type : row.entity.type,
                    margin : row.entity.margin,
                    pan_number : row.entity.pan_number,
                    adhar_number : row.entity.adhar_number,
                    gst_details : row.entity.gst_details,
                    phone_number : row.entity.phone_number,
                    point_of_contact : row.entity.point_of_contact,
                    address : row.entity.address,
                    stock : row.entity.stock
                };

                vc.csv_array.push(temp);

            }
        });

        return vc.csv_array;


    }

    function openUpdateDetailsModal() {

        if(vc.selected_vendor.id) {

            vc.vendor_details = vc.selected_vendor;

            ngDialog.open({
                template: 'updateDetailsModal',
                className: 'ngdialog-theme-default',
                showClose: true,
                scope: $scope
            });
        }
        else{
            SweetAlert.swal({
                title: "",
                text: "Please select Host."
            });
        }
    }

    function updateMarginDetails() {

        ngDialog.close({
            template: 'updateMarginModal',
            className: 'ngdialog-theme-default',
            showClose: true,
            scope: $scope
        });

        vc.spinners     = true;

        var send_data = {
            id : vc.selected_vendor.id
        };

        if(vc.selected_vendor.new_margin_type == 'PER'){
            send_data.margin_rate = parseInt(vc.selected_vendor.new_margin_rate);
            vc.selected_vendor.new_margin_rate_absolute = 0;
        }
        else{
            send_data.margin_rate_absolute = parseInt(vc.selected_vendor.new_margin_rate_absolute);
            vc.selected_vendor.new_margin_rate = 0;
        }


        $http({
            method: 'PUT',
            url: mainVm.urlList.flymall + 'vendor/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            vc.spinners = false;

            if(response.is_error){

                var error_msg = "";
                if(typeof(response.message) == 'string'){
                    error_msg = response.message;
                }
                else{
                    error_msg = response.message[0].message;
                }

                SweetAlert.swal({
                    title: "",
                    text: error_msg
                });
            }
            else{

                vc.selected_vendor.margin_rate = vc.selected_vendor.new_margin_rate;
                vc.selected_vendor.margin_rate_absolute = vc.selected_vendor.new_margin_rate_absolute;

                if(vc.selected_vendor.margin_rate <= 0){
                    vc.gridOptionsComplex.data[vc.selected_index]['margin'] = vc.selected_vendor.margin_rate_absolute;
                }
                else{
                    vc.gridOptionsComplex.data[vc.selected_index]['margin'] = vc.selected_vendor.margin_rate + " (%)"
                }

                vc.gridOptionsComplex.data[vc.selected_index]['full_details']['margin_rate'] = vc.selected_vendor.margin_rate;
                vc.gridOptionsComplex.data[vc.selected_index]['full_details']['margin_rate_absolute'] = vc.selected_vendor.margin_rate_absolute;

                SweetAlert.swal({
                    title: "",
                    text: "Updated"
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });


    }

    function UpdateSellerDetails() {

        ngDialog.close({
            template: 'updateDetailsModal',
            className: 'ngdialog-theme-default',
            showClose: true,
            scope: $scope
        });

        vc.spinners     = true;

        var send_data = {
            id : vc.selected_vendor.id,
            "pin_code" : vc.vendor_details.pin_code,
            "address" : vc.vendor_details.address,
            "gst_details" : (vc.vendor_details.gst_details ? vc.vendor_details.gst_details : ''),
            "adhar_number" : (vc.vendor_details.adhar_number ? vc.vendor_details.adhar_number : ''),
            "pan_number" : vc.vendor_details.pan_number,
            "point_of_contact" : vc.vendor_details.point_of_contact,
            "type" : vc.vendor_details.type
        };

        $http({
            method: 'PUT',
            url: mainVm.urlList.flymall + 'vendor/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            vc.spinners = false;

            if(response.is_error){

                var error_msg = "";
                if(typeof(response.message) == 'string'){
                    error_msg = response.message;
                }
                else{
                    error_msg = response.message[0].message;
                }

                SweetAlert.swal({
                    title: "",
                    text: error_msg
                });
            }
            else{

                vc.gridOptionsComplex.data[vc.selected_index]['pin_code'] = vc.vendor_details.pin_code;
                vc.gridOptionsComplex.data[vc.selected_index]['address'] = vc.vendor_details.address;
                vc.gridOptionsComplex.data[vc.selected_index]['gst_details'] = vc.vendor_details.gst_details;
                vc.gridOptionsComplex.data[vc.selected_index]['adhar_number'] = vc.vendor_details.adhar_number;
                vc.gridOptionsComplex.data[vc.selected_index]['pan_number'] = vc.vendor_details.pan_number;
                vc.gridOptionsComplex.data[vc.selected_index]['point_of_contact'] = vc.vendor_details.point_of_contact;
                vc.gridOptionsComplex.data[vc.selected_index]['type'] = vc.vendor_details.type;



                SweetAlert.swal({
                    title: "",
                    text: "Updated"
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });



    }

    function openModalUpdateModal() {

        if(vc.selected_vendor.id){

            vc.selected_vendor.new_margin_rate = vc.selected_vendor.margin_rate;
            vc.selected_vendor.new_margin_rate_absolute = vc.selected_vendor.margin_rate_absolute;

            if(vc.selected_vendor.margin_rate > 0){
                vc.selected_vendor.new_margin_type = 'PER';
            }
            else{
                vc.selected_vendor.new_margin_type = 'ABS';
            }

            ngDialog.open({
                template: 'updateMarginModal',
                className: 'ngdialog-theme-default',
                showClose: true,
                scope: $scope
            });
        }
        else{
            SweetAlert.swal({
                title: "",
                text: "Please select Host."
            });
        }

    }

    function openReturnConsignmentModal(data) {

        vc.return_consign = data;
        vc.return_consign_data = {
            logistic_partner : 'Any'
        };


        if(vc.internal_vendors.length == 0){

            vc.spinners     = true;

            $http({
                method: 'GET',
                url: mainVm.urlList.flymall + 'vendor/?type=FSTR,WRHS',
                headers: mainVm.headers
            }).success(function (response) {

                var res = mainServices.nodeCheckErrorStatus(response.status);

                if(!res){
                    return false;
                }

                vc.internal_vendors = response.data;
                vc.spinners         = false;

                ngDialog.open({
                    template: 'ReturnConsignmentModal',
                    className: 'ngdialog-theme-default',
                    showClose: true,
                    scope: $scope
                });

            });

        }
        else{
            ngDialog.open({
                template: 'ReturnConsignmentModal',
                className: 'ngdialog-theme-default',
                showClose: true,
                scope: $scope
            });
        }

    }

    function changeDate() {
        $('.confirm-date-div input').removeClass('parsley-error');
        $('.confirm-date-div ul').css('display','none');
    }

    function createReturnConsignment() {

        var send_data = {
            "destination_vendor" : parseInt(vc.return_consign_data.selected_destination),
            "items" : vc.return_consign.stock_items,
            "to_be_received_on" : vc.return_consign_data.date,
            "logistic_partner" : vc.return_consign_data.logistic_partner,
            "created_by" : $localStorage.fly_amazon.email,
            "waybill" : (vc.return_consign_data.waybill ? vc.return_consign_data.waybill : ''),
            "source_vendor" : parseInt(vc.return_consign.id),
            "type" : 'RTD',
            "comment" : vc.return_consign_data.comment
        };

        ngDialog.close({
            template: 'ReturnConsignmentModal',
            className: 'ngdialog-theme-default',
            showClose: true,
            scope: $scope
        });
        vc.spinners     = true;

        $http({
            method: 'POST',
            url: mainVm.urlList.flymall + 'consignment/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {

            vc.spinners     = false;

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            if(response.is_error){

                if(typeof(response.message) == 'string'){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    },function () {
                        ngDialog.open({
                            template: 'ReturnConsignmentModal',
                            className: 'ngdialog-theme-default',
                            showClose: true,
                            scope: $scope
                        });
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message[0]
                    },function () {
                        ngDialog.open({
                            template: 'ReturnConsignmentModal',
                            className: 'ngdialog-theme-default',
                            showClose: true,
                            scope: $scope
                        });
                    });
                }

            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Created"
                },function () {
                    $state.reload();
                });
            }

        });

    }

}


angular
    .module('flyrobe')
    .controller('vendorCtrl', vendorCtrl);
