/**
 ********** Created by Surbhi Goel ************
 **/

function manufactureCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                         mainServices,ngDialog) {

    mf = this;
    mf.panel = "MCH";
    mf.selected_merchant = {
        id : ''
    };

    if(!$filter('tab')(mf.panel)) {
        $state.go('login');
    }


    mainVm.flyrobeState     = "Merchant";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    mf.getData                          = getData;
    mf.exportToCsv                      = exportToCsv;
    mf.openEditManufactureModal         = openEditManufactureModal;
    mf.updateMerchant                   = updateMerchant;

    mf.getData(); // Default call

    function getData() {

        mf.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'manufacturer/',
            headers: mainVm.headers
        }).success(function (response) {
            
            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }
            
            mf.spinners = false;
            table_data = response.data;

            if(table_data.length){

                //column def
                {
                    var column = [];
                    mf.csv_header = [];

                    var col_name = {
                        name : 'checkbox',
                        displayName: '',
                        width : 40,
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ' +
                        'ng-click="grid.appScope.mf.selected_merchant = row.entity;grid.appScope.mf.active_row_index = row.entity.index">' +
                        '<label class="needsclick"> ' +
                        '<i class="fa fa-circle-o" ng-hide="grid.appScope.mf.selected_merchant.id == row.entity.full_info.id"></i> ' +
                        '<i class="fa fa-check-circle-o" ng-show="grid.appScope.mf.selected_merchant.id == row.entity.full_info.id"></i> ' +
                        '</label> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'id',
                        displayName: 'ID',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    mf.csv_header.push('ID');


                    var col_name = {
                        name : 'created_at',
                        displayName: 'Created At',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    mf.csv_header.push('Created At');

                    var col_name = {
                        name : 'user_name',
                        displayName: 'User Name',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    mf.csv_header.push('User Name');

                    var col_name = {
                        name : 'name',
                        displayName: 'Merchant',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    mf.csv_header.push('Merchant');

                    var col_name = {
                        name : 'email',
                        displayName: 'Email',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    mf.csv_header.push('Email');

                    var col_name = {
                        name : 'phone_number',
                        displayName: 'Phone Number',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    mf.csv_header.push('Phone Number');

                    var col_name = {
                        name : 'address',
                        displayName: 'Address',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    mf.csv_header.push('Address');

                    var col_name = {
                        name : 'type_purchase',
                        displayName: 'Purchase Type',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    mf.csv_header.push('Type Purchase');

                    var col_name = {
                        name : 'gst_number',
                        displayName: 'GST',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    mf.csv_header.push('GST');

                }

                mf.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 70,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        mf.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){

                        if(row.entity.full_info.id == mf.selected_merchant.id){
                            return 'ui-grid-active-row';
                        }
                    },
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                table_data.forEach(function (res) {
                    
                    var temp = {
                        created_at : $filter('date')(new Date(res.created_at),'yyyy-MM-dd'),
                        user_name : res.user_name,
                        full_info : res,
                        name : res.first_name + ' ' + res.last_name,
                        id : res.id,
                        email : res.email,
                        phone_number : res.phone_number,
                        address : res.address,
                        type_purchase : res.type_purchase,
                        gst_number : res.gst_number
                    };

                    mf.gridOptionsComplex.data.push(temp);

                });

            }
            else{
                mf.gridOptionsComplex = {
                    data : []
                };
            }

        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function openEditManufactureModal() {

        if(!mf.selected_merchant.id){
            SweetAlert.swal({
                title: "",
                text: "Please select Merchant."
            });
        }
        else{
            mf.selected_merchant.updated_gst = mf.selected_merchant.gst_number;
            ngDialog.open({
                template: 'editManufactureModal',
                className: 'ngdialog-theme-default consignment-view-modal',
                showClose: false,
                scope: $scope
            });
        }

    }

    function exportToCsv() {

        mf.csv_array = [];

        mf.gridApi.grid.rows.forEach(function (row) {
            if (row.visible) {

                var temp = {
                    id : row.entity.id,
                    created_at : row.entity.created_at,
                    user_name : row.entity.user_name,
                    name : row.entity.name,
                    email : row.entity.email,
                    phone_number : row.entity.phone_number,
                    address : row.entity.address,
                    type_purchase : row.entity.type_purchase,
                    gst_number : row.entity.gst_number
                };

                mf.csv_array.push(temp);

            }
        });

        return mf.csv_array;


    }
    
    function updateMerchant() {

        ngDialog.close({
            template: 'editManufactureModal',
            className: 'ngdialog-theme-default consignment-view-modal',
            showClose: false,
            scope: $scope
        });

        $http({
            method: 'PUT',
            url: mainVm.urlList.flymall + 'manufacturer/',
            data : {
                "id" : mf.selected_merchant.id,
                "type_purchase" : mf.selected_merchant.type_purchase,
                "city" : mf.selected_merchant.full_info.city,
                "gst_number" : mf.selected_merchant.updated_gst
            },
            headers: mainVm.headers
        }).success(function (response) {

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: "Something went wrong"
                },function () {
                    ngDialog.open({
                        template: 'editManufactureModal',
                        className: 'ngdialog-theme-default consignment-view-modal',
                        showClose: false,
                        scope: $scope
                    });
                });
            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Updated"
                });
                mf.selected_merchant.gst_number = mf.selected_merchant.updated_gst;
            }

        });
        
    }

}


angular
    .module('flyrobe')
    .controller('manufactureCtrl', manufactureCtrl);