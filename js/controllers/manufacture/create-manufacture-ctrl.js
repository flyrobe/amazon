/**
 ********** Created by Surbhi Goel ************
 **/

function createManufactureCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                         mainServices) {

    cmCtrl = this;
    cmCtrl.panel = "MCH";
    cmCtrl.manufacture = {margin_rate:20}

    if(!$filter('tab')(cmCtrl.panel)) {
        $state.go('login');
    }


    mainVm.flyrobeState     = "Create Merchant";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    cmCtrl.createManufacture                          = createManufacture;


    function createManufacture() {

        cmCtrl.spinners     = true;

        $http({
            method: 'POST',
            url: mainVm.urlList.flymall + 'manufacturer/',
            data : {
                "first_name" : cmCtrl.manufacture.first_name,
                "last_name" : cmCtrl.manufacture.last_name,
                "gst_number" : cmCtrl.manufacture.gst,
                "pan_number" : (cmCtrl.manufacture.pan_no ? cmCtrl.manufacture.pan_no : ''),
                "type_purchase" : cmCtrl.manufacture.type_purchase,
                "user_name" : cmCtrl.manufacture.user_name,
                "email" : cmCtrl.manufacture.email,
                "phone_number" : cmCtrl.manufacture.phone_no,
                "address" : cmCtrl.manufacture.address,
                "city" : cmCtrl.manufacture.city,
                "margin_rate" : (cmCtrl.manufacture.margin_rate ? parseInt(cmCtrl.manufacture.margin_rate) : 20)
            },
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            cmCtrl.spinners = false;

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: response.message
                });
            }
            else{
                cmCtrl.manufacture = {};
                SweetAlert.swal({
                    title: "",
                    text: response.message
                },function () {
                    $state.go('flyrobe.manufacture');
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

}


angular
    .module('flyrobe')
    .controller('createManufactureCtrl', createManufactureCtrl);
