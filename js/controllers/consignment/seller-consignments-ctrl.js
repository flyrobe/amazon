/**
 ********** Created by Surbhi Goel ************
 **/

function sellerConsignmentCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert, mainServices,DTOptionsBuilder,
                         ngDialog) {

    scCtrl = this;
    scCtrl.panel = "CONS";

    if(!$filter('tab')(scCtrl.panel)) {
        $state.go('login');
    }

    scCtrl.spinners     = true;
    scCtrl.show_rhs     = false;

    scCtrl.selected_items     = [];


    mainVm.flyrobeState     = "Host Package";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    scCtrl.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withDisplayLength(50)
        .withOption('order', [2, 'asc'])
        .withButtons([
            {extend: 'copy'}
        ]);// dataTable Searching , Sorting & pagination


    scCtrl.exportToCsv                 = exportToCsv;
    scCtrl.getConsignmentList          = getConsignmentList;
    scCtrl.getConsignmentDetails       = getConsignmentDetails;
    scCtrl.openModalForImage           = openModalForImage;
    scCtrl.selectItem                  = selectItem;
    scCtrl.updateStatus                = updateStatus;
    scCtrl.getVendorList               = getVendorList;


    scCtrl.getVendorList(); //Default Call

    function getConsignmentList() {


        scCtrl.spinners     = true;
        scCtrl.show_rhs     = false;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'consignment/?destination_vendor=' + scCtrl.selected_seller.id +
            '&project=id,grand_price,is_active,received_items,received_on,reserve_for_days,sent_items,should_be_return_on,status',
            headers: mainVm.headers
        }).success(function (response) {

            scCtrl.spinners     = false;

            if(response.data.length){

                //column def
                {
                    var column = [];
                    scCtrl.csv_header = [];

                    var col_name = {
                        name : 'status',
                        displayName: 'Status',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.scCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    scCtrl.csv_header.push('Status');

                    var col_name = {
                        name : 'grand_price',
                        displayName: 'Grand Price',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.scCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    scCtrl.csv_header.push('Grand Price');

                    var col_name = {
                        name : 'is_active',
                        displayName: 'Active',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.scCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    scCtrl.csv_header.push('Active');

                    var col_name = {
                        name : 'received_items',
                        displayName: 'Received Items',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.scCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    scCtrl.csv_header.push('Received Items');

                    var col_name = {
                        name : 'received_on',
                        displayName: 'Received On',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.scCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    scCtrl.csv_header.push('Received On');

                    var col_name = {
                        name : 'reserve_for_days',
                        displayName: 'Days',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.scCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    scCtrl.csv_header.push('Days');

                    var col_name = {
                        name : 'sent_items',
                        displayName: 'Sent Items',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.scCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    scCtrl.csv_header.push('Sent Items');

                    var col_name = {
                        name : 'should_be_return_on',
                        displayName: 'Return',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.scCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    scCtrl.csv_header.push('Return');

                }

                scCtrl.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 100,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        scCtrl.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){
                        if(row.entity.id == scCtrl.active_row_pid){
                            return 'ui-grid-active-row';
                        }
                    },
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                response.data.forEach(function (res) {

                    var temp = {
                        full_info : res,
                        id : res.id,
                        status : res.status,
                        grand_price : res.grand_price,
                        is_active : res.is_active,
                        received_items : res.received_items,
                        received_on : $filter('date')(new Date(res.received_on),'yyyy-MM-dd'),
                        reserve_for_days : res.reserve_for_days,
                        sent_items : res.sent_items,
                        should_be_return_on : $filter('date')(new Date(res.should_be_return_on),'yyyy-MM-dd')
                    };

                    scCtrl.gridOptionsComplex.data.push(temp);

                });
            }
            else{
                scCtrl.gridOptionsComplex = {
                    data : []
                };
            }

        });

    }

    function exportToCsv() {

        scCtrl.csv_array = [];
        scCtrl.gridApi.grid.rows.forEach(function (row) {
            if (row.visible) {

                var temp = {
                    status : row.entity.status,
                    grand_price : row.entity.grand_price,
                    is_active : row.entity.is_active,
                    received_items : row.entity.received_items,
                    received_on : row.entity.received_on,
                    reserve_for_days : row.entity.reserve_for_days,
                    sent_items : row.entity.sent_items,
                    should_be_return_on : row.entity.should_be_return_on
                };

                scCtrl.csv_array.push(temp);

            }
        });

        return scCtrl.csv_array;


    }

    function getConsignmentDetails(id) {

        scCtrl.active_row_pid = id;
        scCtrl.selected_items = [];

        scCtrl.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'consignment?id=' + id,
            headers: mainVm.headers
        }).success(function (response) {

            scCtrl.spinners     = false;
            scCtrl.show_rhs     = true;

            scCtrl.rhs_data = response.data;

        });



    }

    function selectItem(item,type) {

        if(type == 'all'){

            if(scCtrl.checked){
                scCtrl.selected_items = [];
                item.forEach(function (col) {
                    col.checked = false;
                });
            }
            else{

                var common_status = true;

                item.forEach(function (col) {
                    if( col.status != 'SOT' && col.consignment_state != item[0].consignment_state){
                        common_status = false;
                    }
                });

                if(!common_status){
                    SweetAlert.swal({
                        title: "",
                        text: "Selected items must have common state."
                    });
                    return false;
                }
                else{
                    item.forEach(function (col) {
                        if(col.status != 'SOT'){
                            scCtrl.selected_items.push(col);
                            col.checked = true;
                        }
                    });
                }
            }

            scCtrl.checked = !scCtrl.checked;

        }
        else{ //single selection
            if(item.checked){
                item.checked = false;

                var i = scCtrl.selected_items.indexOf(item);
                if (i != -1) {
                    scCtrl.selected_items.splice(i, 1);
                }

            }
            else{

                if(scCtrl.selected_items.length > 0 && item.consignment_state != scCtrl.selected_items[0].consignment_state){
                    SweetAlert.swal({
                        title: "",
                        text: "Selected items must have common state."
                    });
                    return false;
                }

                item.checked = true;

                var i = scCtrl.selected_items.indexOf(item);
                if (i == -1) {
                    scCtrl.selected_items.push(item);
                }
            }
        }//end of main else

    }

    function updateStatus(type) {

        if(scCtrl.selected_items.length <= 0){
            SweetAlert.swal({
                title: "",
                text: "Please select atleast one item."
            });
            return false;
        }

        if(type == 'RCD'){
            if(scCtrl.selected_items[0].consignment_state != 'SHP'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }

            var sent_items = [];
            scCtrl.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            scCtrl.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/consignment/received',
                data : {
                    "consignment_id" : scCtrl.rhs_data.id,
                    "access_token" : $localStorage.fly_amazon.user_info.access_token,
                    "item_list" : sent_items,
                    "marked_by_us" : true
                },
                headers: mainVm.headers
            }).success(function (response) {

                scCtrl.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    scCtrl.getConsignmentDetails(scCtrl.rhs_data.id);
                }


            });


        }
        else if(type == 'NRCD'){
            if(scCtrl.selected_items[0].consignment_state != 'SHP'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }

            var sent_items = [];
            scCtrl.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            scCtrl.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/consignment/not-received',
                data : {
                    "consignment_id" : scCtrl.rhs_data.id,
                    "access_token" : $localStorage.fly_amazon.user_info.access_token,
                    "item_list" : sent_items,
                    "marked_by_us" : true
                },
                headers: mainVm.headers
            }).success(function (response) {

                scCtrl.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    scCtrl.getConsignmentDetails(scCtrl.rhs_data.id);
                }


            });


        }
        else{
            if(scCtrl.selected_items[0].consignment_state != 'RCD'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }


            var sent_items = [];
            scCtrl.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            scCtrl.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/item/mark-sold-out',
                data : {
                    "consignment_id" : scCtrl.rhs_data.id,
                    "access_token" : $localStorage.fly_amazon.user_info.access_token,
                    "item_list" : sent_items,
                    "vendor_id" : scCtrl.selected_seller.id,
                    "marked_by_us" : true
                },
                headers: mainVm.headers
            }).success(function (response) {

                scCtrl.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    scCtrl.getConsignmentDetails(scCtrl.rhs_data.id);
                }


            });

        }

    }

    function openModalForImage(url) {
        scCtrl.imagePath = url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }

    function getVendorList() {

        scCtrl.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'vendor/',
            headers: mainVm.headers
        }).success(function (response) {

            scCtrl.vendor_list = [];
            scCtrl.spinners    = false;

            response.data.forEach(function (col) {

                if(col.type != 'WRHS'){
                    scCtrl.vendor_list.push(col);
                }

            });


        });

    }

}


angular
    .module('flyrobe')
    .controller('sellerConsignmentCtrl', sellerConsignmentCtrl);
