/**
 ********** Created by Surbhi Goel ************
 **/

function returnConsignmentCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                               mainServices,ngDialog) {

    rc = this;
    rc.panel = "RTD";

    if(!$filter('tab')(rc.panel)) {
        $state.go('login');
    }


    rc.spinner = true;
    $scope.checkall = false;

    rc.type_list = [
        { 'id' : 'RTD', 'name' : 'Return By Trunkshow'},
        { 'id' : 'RTDH', 'name' : 'Return By Host'}
    ];
    rc.logistic_partner_list = [
        { 'id' : 'Any', 'name' : 'Any'}
    ];

    rc.vendor_list = [];
    rc.destination_list = [];
    rc.selected_items = [];
    rc.cons = {
        type : rc.type_list[0].id,
        logistic_partner : 'Any'
    };
    rc.selected_seller = "";
    rc.selected_show=null;
    rc.trunk_show_list = []


    mainVm.flyrobeState     = "Create Return Package";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    rc.changeConfirmDate            = changeConfirmDate;
    rc.createConsignment            = createConsignment;
    rc.getItemList                  = getItemList;
    rc.getManufacturerList          = getManufacturerList;
    rc.getVendorList                = getVendorList;
    rc.openModalForImage            = openModalForImage;
    rc.selectCheckBox               = selectCheckBox;
    rc.selectCheckBoxAll            = selectCheckBoxAll;
    rc.itemPush                     = itemPush;
    rc.itemPop                      = itemPop;
    rc.getTrunkShowList             = getTrunkShowList;

    rc.getVendorList(); //Default Call
    rc.getTrunkShowList()

    function createConsignment() {

        rc.spinners     = true;

        var items = [];
        if(rc.cons.items && rc.cons.items.length){
            items = rc.cons.items.split(',');
        }

        var send_data = {
            "destination_vendor" : parseInt(rc.selected_destination),
            "items" : [],
            "to_be_received_on" : rc.cons.date,
            "logistic_partner" : rc.cons.logistic_partner,
            "created_by" : $localStorage.fly_amazon.email,
            "waybill" : (rc.cons.waybill ? rc.cons.waybill : ''),
            "source_vendor" : (rc.selected_show ? parseInt(rc.selected_show.split('-')[1]) : parseInt(rc.selected_host.split('-')[0])),
            "type" : 'RTD',
            "comment" : rc.cons.comment
        };
        var trunk_show_event_id = rc.selected_show ? rc.selected_show.split('-')[0] : 5;
        console.log(trunk_show_event_id);
        if (trunk_show_event_id){
            send_data['trunk_show_event_id'] = parseInt(trunk_show_event_id)
        }

        //manual items
        items.forEach(function (col) {
            send_data.items.push(parseInt(col));
        });

        //selected items
        rc.selected_items.forEach(function (col) {
            send_data.items.push(parseInt(col));
        });

        send_data.items = Array.from(new Set(send_data.items));

        $http({
            method: 'POST',
            url: mainVm.urlList.flymall + 'consignment/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            rc.spinners = false;

            if(response.is_error){

                if(typeof(response.message) == 'string'){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message[0]
                    });
                }

            }
            else{
                rc.manufacture = {};
                SweetAlert.swal({
                    title: "",
                    text: "Created"
                },function () {
                    rc.selected_items = [];
                    $state.go('flyrobe.consignment');
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function changeConfirmDate() {
        $('.confirm-date-div input').removeClass('parsley-error');
        $('.confirm-date-div ul').css('display','none');
    }

    function getVendorList() {

        rc.spinners     = true;
        rc.vendor_list  = [];
        rc.vendor_list_2 = {};

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'vendor/',
            headers: mainVm.headers
        }).success(function (response) {

            // rc.getManufacturerList();

            // rc.vendor_list = response.data;
            // rc.vendor_list_2 = response.data;
            response.data.forEach(function (col) {

                if(col.type != 'WRHS'){
                    rc.vendor_list.push(col);
                    rc.vendor_list_2[col.id] = col.user_name;
                }
                else{
                    rc.destination_list.push(col);
                }

            });
            rc.spinners    = false;

        });

        console.log(rc.vendor_list)

    }

    function getTrunkShowList() {
        rc.spinners     = true;
        $http({
            method: 'GET',
            url: mainVm.urlList.flymall2 + 'trunk-show-event/',
            headers: mainVm.headers
        }).success(function (res) {
            rc.trunk_show_list = res.data;
            rc.spinners    = false;
        })
    }

    function getManufacturerList() {

        rc.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'manufacturer/',
            headers: mainVm.headers
        }).success(function (response) {

            rc.manufacturer_list = {};
            rc.manufacturer_list_2 = response.data;
            response.data.forEach(function (col) {
                rc.manufacturer_list[col.id] = col.user_name;
            });

            rc.spinners     = false;

        });

    }

    function getItemList(id) {

        rc.spinners     = true;
        rc.selected_items = [];

        console.log(id)
        console.log(rc.selected_host)
        if(id == 'ho'){
          rc.selected_show = null;
        }else{
          rc.selected_host = null
        }

        if(rc.selected_show) {
          var manuf_id = rc.selected_show.split('-')[1];
        }else{
          var manuf_id = rc.selected_host.split('-')[0];
        }
        // if(rc.selected_seller == 'all'){
        //     manuf_id = "";
        // }

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall2 + 'catalog/items/?status=STS&vendor_id=' + manuf_id,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            rc.spinners = false;
            table_data = response.data;

            if(table_data.length){

                //column def
                {
                    var column = [];
                    rc.csv_header = [];

                    var col_name = {
                        name : 'full_info',
                        displayName: '',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="checkbox c-checkbox needsclick p-8 h-100p"' +
                        'ng-click="grid.appScope.rc.selectCheckBox(COL_FIELD)"> ' +
                        '<label class="needsclick"> ' +
                        '<i class="fa fa-square-o" ng-hide="row.entity.full_info.checked"></i> ' +
                        '<i class="fa fa-check-square-o" ng-show="row.entity.full_info.checked"></i> ' +
                        '</label> ' +
                        '</div>',
                        headerCellTemplate:  '' +
                        '<div class="checkbox c-checkbox needsclick p-8 h-100p"' +
                        'ng-click="grid.appScope.rc.selectCheckBoxAll()"> ' +
                        '<label class="needsclick"> ' +
                        '<i class="fa fa-square-o" ng-hide="grid.appScope.checkall"></i>' +
                        '<i class="fa fa-check-square-o" ng-show="grid.appScope.checkall"></i>' +
                        '</label> ' +
                        '</div>',
                    };
                    column.push(col_name);
                    rc.csv_header.push('Id');

                    var col_name = {
                        name : 'id',
                        displayName: 'Id',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rc.csv_header.push('Id');

                    var col_name = {
                        name: 'image',
                        displayName: 'Image',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p"> ' +
                        '<img src="{{COL_FIELD}}" alt="Image" ng-click="grid.appScope.rc.openModalForImage(row.entity.image2)"' +
                        'class="img-responsive img-circle"' +
                        'style="height: 40px; width: 25px; cursor: pointer;"/> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'item_code',
                        displayName: 'Item Code',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rc.csv_header.push('Item Code');

                    var col_name = {
                        name : 'manufacturer',
                        displayName: 'Merchant',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rc.csv_header.push('Merchant');

                    var col_name = {
                        name : 'size',
                        displayName: 'Size',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rc.csv_header.push('Size');

                    var col_name = {
                        name : 'status',
                        displayName: 'Status',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rc.csv_header.push('Status');

                    var col_name = {
                        name : 'whats_included',
                        displayName: 'Whats Included',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rc.csv_header.push('Whats Included');


                }

                rc.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 70,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        rc.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){},
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                table_data.forEach(function (res) {

                    var temp = {
                        full_info : res,
                        id : res.id,
                        item_code : res.item_code,
                        vendor : rc.vendor_list_2[res.vendor_id],
                        size : res.size,
                        image : mainVm.urlList.imagekit_100_url + res.style_info.image_front,
                        image2 : mainVm.urlList.imagekit_url + res.style_info.image_front,
                        status : res.status,
                        whats_included : res.style_info.whats_included
                    };

                    if(res.style_info.image_front.indexOf('http') >= 0){
                        temp.image = res.style_info.image_front;
                        temp.image2 = res.style_info.image_front;
                    }

                    if(temp.size == 'STS'){
                        temp.size = "STH";
                    }

                    rc.gridOptionsComplex.data.push(temp);

                });

            }
            else{
                rc.gridOptionsComplex = {
                    data : []
                };
            }

        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function selectCheckBoxAll() {

        if(!$scope.checkall){
          rc.gridOptionsComplex.data.map((item) => {
            rc.itemPush(item.full_info);
          })
          $scope.checkall = true;
        }else{
          rc.gridOptionsComplex.data.map((item) => {
            rc.itemPop(item.full_info);
          })
          $scope.checkall = false;
        }

    }

    function selectCheckBox(col) {
        if(col.checked){
            rc.itemPop(col)
        }
        else{
            rc.itemPush(col);
        }
    }

    function itemPop (col) {
      col.checked = false;

      var i = rc.selected_items.indexOf(col.id);

      if (i != -1) {
          $scope.checkall = false;
          rc.selected_items.splice(i, 1);
      }
    }

    function itemPush (col) {

      col.checked = true;
      var i = rc.selected_items.indexOf(col.id);

      if (i == -1) {
          rc.selected_items.push(col.id);
      }

      if(rc.gridOptionsComplex.data.length == rc.selected_items.length){
        $scope.checkall = true;
      }
      // else{
      //     SweetAlert.swal({
      //         title: "",
      //         text: "Already added"
      //     });
      // }
    }

    function openModalForImage(url) {
        rc.imagePath = url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }


}


angular
    .module('flyrobe')
    .controller('returnConsignmentCtrl', returnConsignmentCtrl);
