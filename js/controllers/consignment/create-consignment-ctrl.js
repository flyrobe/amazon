/**
 ********** Created by Surbhi Goel ************
 **/

function createConsignmentCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                           mainServices,ngDialog) {

    con = this;
    con.panel = "CONS";

    if(!$filter('tab')(con.panel)) {
        $state.go('login');
    }
    
    
    con.spinner = true;


    con.type_list = [
        { 'id' : 'STS', 'name' : 'Send to Host'}
        // { 'id' : 'PRTD', 'name' : 'Partially Return'},
        // { 'id' : 'RTD', 'name' : 'Return'}
    ];
    con.logistic_partner_list = [
        { 'id' : 'Any', 'name' : 'Any'}
    ];

    con.vendor_list = [];
    con.sender_list = [];
    con.selected_items = [];
    con.scanned_items = [];
    con.selected_to   = {};
    con.trunk_show_event_id = null;
    con.cons = {
        type : con.type_list[0].id
    };
    con.selected_manufacture = "";

    mainVm.flyrobeState     = "Create Package";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    con.changeConfirmDate            = changeConfirmDate;
    con.createConsignment            = createConsignment;
    // con.getItemList                  = getItemList;
    // con.getManufacturerList          = getManufacturerList;
    con.getTODetails                 = getTODetails;
    con.getTOList                    = getTOList;
    con.getVendorMargin              = getVendorMargin;
    con.getVendorList                = getVendorList;
    con.openModalForImage            = openModalForImage;
    con.scanBarcode                  = scanBarcode;
    con.selectCheckBox               = selectCheckBox;

    con.getVendorList(); //Default Call

    function createConsignment() {

        con.spinners     = true;

        /*var items = [];
        if(con.cons.items && con.cons.items.length){
            items = con.cons.items.split(',');
        }*/

        var send_data = {
            "destination_vendor" : (con.cons.vendor && con.cons.vendor.id ? con.cons.vendor.id : 0 ),
            "reserve_for_days" : parseInt(con.selected_to.reserve_for_days ? con.selected_to.reserve_for_days : 1),
            "items" : con.selected_items_id,
            "to_be_received_on" : con.selected_to.to_be_dispatch_at,
            "logistic_partner" : con.cons.logistic_partner,
            "created_by" : $localStorage.fly_amazon.email,
            "waybill" : (con.cons.waybill ? con.cons.waybill : ''),
            "source_vendor" : (con.cons.sender && con.cons.sender.id ? con.cons.sender.id : 0 ),
            "type" : con.cons.type,
            "pick_list_id" : parseInt(con.selected_picklist_id),
            "comment" : con.cons.comment,
            "updated_margin_rate" : con.selected_to.vendor_margin_rate,
            "updated_margin_rate_absolute" : con.selected_to.vendor_margin_rate_absolute,
        };
        if (con.trunk_show_event_id){
            send_data['trunk_show_event_id'] = con.trunk_show_event_id
        }

        /*//manual items
        items.forEach(function (col) {
            send_data.items.push(parseInt(col));
        });

        //selected items
        con.selected_items.forEach(function (col) {
            send_data.items.push(parseInt(col));
        });*/

        send_data.items = Array.from(new Set(send_data.items));

        if(!con.cons.sender || !con.cons.sender.id){
            SweetAlert.swal({
                title: "",
                text: "Please select Sender."
            });
            con.spinners = false;
            return false;
        }

        if(!con.cons.vendor || !con.cons.vendor.id){
            SweetAlert.swal({
                title: "",
                text: "Please select Host."
            });
            con.spinners = false;
            return false;
        }

        if(send_data.items.length <= 0){
            SweetAlert.swal({
                title: "",
                text: "Please select atleast one item."
            });
            con.spinners = false;
            return false;
        }

        /*if(con.cons.new_margin_type == "PER"){
            send_data.updated_margin_rate = parseInt(con.cons.new_margin_rate);
        }
        else{
            send_data.updated_margin_rate_absolute = parseInt(con.cons.new_margin_rate_absolute);
        }*/

        $http({
            method: 'POST',
            url: mainVm.urlList.flymall + 'consignment/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            con.spinners = false;

            if(response.is_error){

                if(typeof(response.message) == 'string'){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message[0]
                    });
                }

            }
            else{
                con.manufacture = {};
                SweetAlert.swal({
                    title: "",
                    text: "Created"
                },function () {
                    con.selected_items = [];
                    $state.go('flyrobe.consignment');
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function changeConfirmDate() {
        $('.confirm-date-div input').removeClass('parsley-error');
        $('.confirm-date-div ul').css('display','none');
    }

    function getVendorList() {

        con.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'vendor/',
            headers: mainVm.headers
        }).success(function (response) {

            // con.getManufacturerList();

            con.vendor_list = [];
            con.vendor_list_2 = {};
            con.sender_list = [];

            response.data.forEach(function (col) {

                con.vendor_list_2[col.id] = col;

                if(col.type != 'WRHS'){
                    con.vendor_list.push(col);
                }
                else{
                    con.sender_list.push(col);
                }

            });

            // con.spinners    = false;
            con.getTOList();

        });

    }

    /*function getManufacturerList() {

        con.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'manufacturer/',
            headers: mainVm.headers
        }).success(function (response) {

            con.manufacturer_list = {};
            con.manufacturer_list_2 = response.data;
            response.data.forEach(function (col) {
                con.manufacturer_list[col.id] = col.user_name;
            });

            con.spinners     = false;

        });

    }*/

    /*function getItemList() {

        con.spinners     = true;
        // con.selected_items = [];

        var manuf_id = con.selected_manufacture;
        if(con.selected_manufacture == 'all'){
            manuf_id = "";
        }

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall2 + 'catalog/items/?manufacturer_id=' + manuf_id
                + "&status=AVL",
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            con.spinners = false;
            table_data = response.data;

            if(table_data.length){

                //column def
                {
                    var column = [];
                    con.csv_header = [];

                    var col_name = {
                        name : 'full_info',
                        displayName: '',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="checkbox c-checkbox needsclick p-8 h-100p"' +
                        'ng-click="grid.appScope.con.selectCheckBox(COL_FIELD)"> ' +
                        '<label class="needsclick"> ' +
                        '<i class="fa fa-square-o" ng-hide="row.entity.full_info.checked"></i> ' +
                        '<i class="fa fa-check-square-o" ng-show="row.entity.full_info.checked"></i> ' +
                        '</label> ' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Id');

                    var col_name = {
                        name : 'id',
                        displayName: 'Id',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Id');

                    var col_name = {
                        name: 'image',
                        displayName: 'Image',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p"> ' +
                        '<img src="{{COL_FIELD}}" alt="Image" ng-click="grid.appScope.con.openModalForImage(row.entity.image2)"' +
                        'class="img-responsive img-circle"' +
                        'style="height: 40px; width: 25px; cursor: pointer;"/> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'item_code',
                        displayName: 'Item Code',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Item Code');

                    var col_name = {
                        name : 'manufacturer',
                        displayName: 'Merchant',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Merchant');

                    var col_name = {
                        name : 'size',
                        displayName: 'Size',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Size');

                    var col_name = {
                        name : 'status',
                        displayName: 'Status',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Status');


                }

                con.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 70,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        con.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){},
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                table_data.forEach(function (res) {

                    var temp = {
                        full_info : res,
                        id : res.id,
                        item_code : res.item_code,
                        manufacturer : con.manufacturer_list[res.manufacturer_id],
                        size : res.size,
                        image : mainVm.urlList.imagekit_100_url + res.style_info.image_front,
                        image2 : mainVm.urlList.imagekit_url + res.style_info.image_front,
                        status : res.status
                    };

                    if(res.style_info.image_front.indexOf('http') >= 0){
                        temp.image = res.style_info.image_front;
                        temp.image2 = res.style_info.image_front;
                    }

                    con.gridOptionsComplex.data.push(temp);

                });

            }
            else{
                con.gridOptionsComplex = {
                    data : []
                };
            }

        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }*/

    function selectCheckBox(col) {

        if(col.checked){
            col.checked = false;

            var i = con.selected_items.indexOf(col.id);

            if (i != -1) {
                con.selected_items.splice(i, 1);
            }

        }
        else{
            col.checked = true;

            var i = con.selected_items.indexOf(col.id);

            if (i == -1) {
                con.selected_items.push(col.id);
            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Already added"
                });
            }
        }

    }

    function openModalForImage(url) {
        con.imagePath = url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }

    function getVendorMargin(vendor) {

        if(vendor.margin_rate > 0){
            con.cons.new_margin_type = "PER";
            con.cons.new_margin_rate = vendor.margin_rate;
        }
        else{
            con.cons.new_margin_type = "ABS";
            con.cons.new_margin_rate_absolute = vendor.margin_rate_absolute;
        }

    }

    function getTOList() {

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'pick-list/',
            headers: mainVm.headers
        }).success(function (response) {

            con.spinners     = false;
            con.to_list = [];

            response.data.forEach(function (col) {
                var temp = {
                    id : col.id,
                    name : col.id + "__" + col.boutique_vendors_info.user_name ,
                    date : $filter('date')(new Date(col.created_at),'dd-MMM-yyyy'),
                    trunk_show_event_id: col.trunk_show_event_id
                };
                con.to_list.push(temp);
            });

        });

    }

    function getTODetails() {

        con.show_vendor_details = false;
        con.scanned_items = [];
        con.selected_items = [];
        con.selected_items_id = [];
        con.selected_to = {};
        con.trunk_show_event_id = null;

        if(!con.picklist_id || !con.picklist_id.id){
            SweetAlert.swal({
                title: "",
                text: "Please enter T.O. Id."
            });
            con.spinners = false;
            return false;
        }

        con.spinner = true;

        $http({
            method: 'GET',
            // url: mainVm.urlList.flymall + 'pick-list/?id=' + con.picklist_id,
            url: mainVm.urlList.flymall + 'pick-list/?id=' + con.picklist_id.id,
            headers: mainVm.headers
        }).success(function (response) {

            con.spinner = false;
            con.selected_picklist_id = con.picklist_id.id;
            con.trunk_show_event_id = con.picklist_id.trunk_show_event_id

            if(response.data.length){

                con.cons.vendor = con.vendor_list_2[response.data[0].destination_vendor];
                con.show_vendor_details = true;
                con.selected_to = response.data[0];
                con.getVendorMargin(con.cons.vendor);


                //column def
                {
                    var column = [];
                    con.csv_header = [];

                    var col_name = {
                        name : 'full_info',
                        displayName: '',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="checkbox c-checkbox needsclick p-8 h-100p">' +
                        '<label class="needsclick"> ' +
                        '<i class="fa fa-square-o" ng-hide="row.entity.full_info.checked"></i> ' +
                        '<i class="fa fa-check-square-o" ng-show="row.entity.full_info.checked"></i> ' +
                        '</label> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'id',
                        displayName: 'Id',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Id');

                    var col_name = {
                        name: 'image',
                        displayName: 'Image',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p"> ' +
                        '<img src="{{COL_FIELD}}" alt="Image" ng-click="grid.appScope.con.openModalForImage(row.entity.image2)"' +
                        'class="img-responsive img-circle"' +
                        'style="height: 40px; width: 25px; cursor: pointer;"/> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'size',
                        displayName: 'Size',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Size');

                    var col_name = {
                        name : 'short_description',
                        displayName: 'Description',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Description');

                    var col_name = {
                        name : 'required_count',
                        displayName: 'Required',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('required');

                    var col_name = {
                        name : 'selected',
                        displayName: 'Selected',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Selected');

                    var col_name = {
                        name : 'available_count',
                        displayName: 'Available',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    con.csv_header.push('Available');


                }

                con.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 70,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        con.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){},
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                response.data[0].parent_items.forEach(function (res) {

                    res.checked = false;

                    var temp = {
                        full_info : res,
                        id : res.items[0].style_info.identification_code,
                        short_description : res.items[0].style_info.short_description,
                        size : res.size,
                        code : res.items[0].style_info.identification_code + "-" + res.size,
                        item_code_arr : [],
                        item_id_arr : [],
                        selected : 0,
                        required_count : res.required_count,
                        available_count : res.items.length,
                        image : mainVm.urlList.imagekit_100_url + res.items[0].style_info.image_front,
                        image2 : mainVm.urlList.imagekit_url + res.items[0].style_info.image_front
                    };

                    if(res.items[0].style_info.image_front.indexOf('http') >= 0){
                        temp.image = res.items[0].style_info.image_front;
                        temp.image2 = res.items[0].style_info.image_front;
                    }

                    res.items.forEach(function (col) {
                        temp.item_code_arr.push(col.item_code);
                        temp.item_id_arr.push(col.id);
                    });

                    con.gridOptionsComplex.data.push(temp);

                });

            }
            else{

                SweetAlert.swal({
                    title: "",
                    text: "You entered wrong id."
                });

                con.gridOptionsComplex = {
                    data : []
                };
            }


        });

    }

    function scanBarcode(barcode,type) {

        var error = false;
        con.barcode = "";

        barcode = barcode.replace(/ /g, "");

        if(!type){
            barcode = barcode.substr(0,barcode.length-1);
        }

        con.scanned_items.forEach(function (id) {
            if(id.trim() == barcode.trim()){
                error = true;
            }
        });

        if(error){
            SweetAlert.swal({
                title: "",
                text: "Duplicate Item"
            });
            return true;
        }
        else{

            var found = false;
            con.scanned_items.push(barcode);

            for(var j = 0; j < con.gridOptionsComplex.data.length && !found; j++){
                var col = con.gridOptionsComplex.data[j];

                if(barcode.indexOf(col.code) >= 0){

                    var len = col.item_code_arr.length;

                    for(var i = 0; i < col.item_code_arr.length && !found; i++){
                        if(col.item_code_arr[i] == barcode){
                            found = true;
                            col.selected = 1;
                            col.full_info.checked = true;
                            con.selected_items.push(barcode);
                            con.selected_items_id.push(col.item_id_arr[i]);
                        }
                    }

                }


            }

            /*con.gridOptionsComplex.data.forEach(function (col) {

                if(barcode.indexOf(col.code) >= 0){

                    var len = col.item_code_arr.length;

                    for(var i = 0; i < col.item_code_arr.length && !found; i++){
                        if(col.item_code_arr[i] == barcode){
                            found = true;
                            col.selected = 1;
                            col.full_info.checked = true;
                            con.selected_items.push(barcode);
                            con.selected_items_id.push(col.item_id_arr[i]);
                        }
                    }

                }

            });*/

            if(!found){
                SweetAlert.swal({
                    title: "",
                    text: "Wrong Item scanned."
                });
                return true;
            }

        }

    }

    $scope.$on('$destroy', function() {
        con.vendor_list = [];
        con.sender_list = [];
        con.selected_items = [];
        con.scanned_items = [];
    });




}


angular
    .module('flyrobe')
    .controller('createConsignmentCtrl', createConsignmentCtrl);