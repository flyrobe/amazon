/**
 ********** Created by Surbhi Goel ************
 **/

function sellerReturnConsignmentCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert, mainServices,DTOptionsBuilder,
                               ngDialog) {

    srCtrl = this;
    srCtrl.panel = "RTD";

    if(!$filter('tab')(srCtrl.panel)) {
        $state.go('login');
    }

    srCtrl.spinners     = true;
    srCtrl.show_rhs     = false;

    srCtrl.selected_items     = [];


    mainVm.flyrobeState     = "Host Return Package";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    srCtrl.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withDisplayLength(50)
        .withOption('order', [2, 'asc'])
        .withButtons([
            {extend: 'copy'}
        ]);// dataTable Searching , Sorting & pagination


    srCtrl.exportToCsv                 = exportToCsv;
    srCtrl.getConsignmentList          = getConsignmentList;
    srCtrl.getConsignmentDetails       = getConsignmentDetails;
    srCtrl.openModalForImage           = openModalForImage;
    srCtrl.selectItem                  = selectItem;
    srCtrl.updateStatus                = updateStatus;
    srCtrl.getVendorList               = getVendorList;


    srCtrl.getVendorList(); //Default Call

    function getConsignmentList() {


        srCtrl.spinners     = true;
        srCtrl.show_rhs     = false;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'consignment/?source_vendor=' + srCtrl.selected_seller.id +
            '&type=RTD&project=id,grand_price,is_active,received_items,received_on,reserve_for_days,sent_items,should_be_return_on,status',
            headers: mainVm.headers
        }).success(function (response) {

            srCtrl.spinners     = false;

            if(response.data.length){

                //column def
                {
                    var column = [];
                    srCtrl.csv_header = [];

                    var col_name = {
                        name : 'status',
                        displayName: 'Status',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.srCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    srCtrl.csv_header.push('Status');

                    var col_name = {
                        name : 'grand_price',
                        displayName: 'Grand Price',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.srCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    srCtrl.csv_header.push('Grand Price');

                    var col_name = {
                        name : 'is_active',
                        displayName: 'Active',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.srCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    srCtrl.csv_header.push('Active');

                    var col_name = {
                        name : 'received_items',
                        displayName: 'Received Items',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.srCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    srCtrl.csv_header.push('Received Items');

                    var col_name = {
                        name : 'received_on',
                        displayName: 'Received On',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.srCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    srCtrl.csv_header.push('Received On');

                    var col_name = {
                        name : 'reserve_for_days',
                        displayName: 'Days',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.srCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    srCtrl.csv_header.push('Days');

                    var col_name = {
                        name : 'sent_items',
                        displayName: 'Sent Items',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.srCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    srCtrl.csv_header.push('Sent Items');

                    var col_name = {
                        name : 'should_be_return_on',
                        displayName: 'Return',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ng-click="grid.appScope.srCtrl.getConsignmentDetails(row.entity.id)">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    srCtrl.csv_header.push('Return');

                }

                srCtrl.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 100,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        srCtrl.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){
                        if(row.entity.id == srCtrl.active_row_pid){
                            return 'ui-grid-active-row';
                        }
                    },
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                response.data.forEach(function (res) {

                    var temp = {
                        full_info : res,
                        id : res.id,
                        status : res.status,
                        grand_price : res.grand_price,
                        is_active : res.is_active,
                        received_items : res.received_items,
                        received_on : $filter('date')(new Date(res.received_on),'yyyy-MM-dd'),
                        reserve_for_days : res.reserve_for_days,
                        sent_items : res.sent_items,
                        should_be_return_on : $filter('date')(new Date(res.should_be_return_on),'yyyy-MM-dd')
                    };

                    srCtrl.gridOptionsComplex.data.push(temp);

                });
            }
            else{
                srCtrl.gridOptionsComplex = {
                    data : []
                };
            }

        });

    }

    function exportToCsv() {

        srCtrl.csv_array = [];
        srCtrl.gridApi.grid.rows.forEach(function (row) {
            if (row.visible) {

                var temp = {
                    status : row.entity.status,
                    grand_price : row.entity.grand_price,
                    is_active : row.entity.is_active,
                    received_items : row.entity.received_items,
                    received_on : row.entity.received_on,
                    reserve_for_days : row.entity.reserve_for_days,
                    sent_items : row.entity.sent_items,
                    should_be_return_on : row.entity.should_be_return_on
                };

                srCtrl.csv_array.push(temp);

            }
        });

        return srCtrl.csv_array;


    }

    function getConsignmentDetails(id) {

        srCtrl.active_row_pid = id;
        srCtrl.selected_items = [];

        srCtrl.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'consignment?id=' + id,
            headers: mainVm.headers
        }).success(function (response) {

            srCtrl.spinners     = false;
            srCtrl.show_rhs     = true;

            srCtrl.rhs_data = response.data;

        });



    }

    function selectItem(item,type) {

        if(type == 'all'){

            if(srCtrl.checked){
                srCtrl.selected_items = [];
                item.forEach(function (col) {
                    col.checked = false;
                });
            }
            else{

                var common_status = true;

                item.forEach(function (col) {
                    if( col.status != 'SOT' && col.consignment_state != item[0].consignment_state){
                        common_status = false;
                    }
                });

                if(!common_status){
                    SweetAlert.swal({
                        title: "",
                        text: "Selected items must have common state."
                    });
                    return false;
                }
                else{
                    item.forEach(function (col) {
                        if(col.status != 'SOT'){
                            srCtrl.selected_items.push(col);
                            col.checked = true;
                        }
                    });
                }
            }

            srCtrl.checked = !srCtrl.checked;

        }
        else{ //single selection
            if(item.checked){
                item.checked = false;

                var i = srCtrl.selected_items.indexOf(item);
                if (i != -1) {
                    srCtrl.selected_items.splice(i, 1);
                }

            }
            else{

                if(srCtrl.selected_items.length > 0 && item.consignment_state != srCtrl.selected_items[0].consignment_state){
                    SweetAlert.swal({
                        title: "",
                        text: "Selected items must have common state."
                    });
                    return false;
                }

                item.checked = true;

                var i = srCtrl.selected_items.indexOf(item);
                if (i == -1) {
                    srCtrl.selected_items.push(item);
                }
            }
        }//end of main else

    }

    function updateStatus(type) {

        if(srCtrl.selected_items.length <= 0){
            SweetAlert.swal({
                title: "",
                text: "Please select atleast one item."
            });
            return false;
        }

        if(type == 'RCD'){
            /*if(srCtrl.selected_items[0].consignment_state != 'SHP'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }*/

            var sent_items = [];
            srCtrl.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            srCtrl.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/consignment/received',
                data : {
                    "consignment_id" : srCtrl.rhs_data.id,
                    "access_token" : $localStorage.fly_amazon.user_info.access_token,
                    "item_list" : sent_items,
                    "marked_by_us" : true
                },
                headers: mainVm.headers
            }).success(function (response) {

                srCtrl.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    srCtrl.getConsignmentDetails(srCtrl.rhs_data.id);
                }


            });


        }
        else if(type == 'NRCD'){
            /*if(srCtrl.selected_items[0].consignment_state != 'SHP'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }*/

            var sent_items = [];
            srCtrl.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            srCtrl.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/consignment/not-received',
                data : {
                    "consignment_id" : srCtrl.rhs_data.id,
                    "access_token" : $localStorage.fly_amazon.user_info.access_token,
                    "item_list" : sent_items,
                    "marked_by_us" : true
                },
                headers: mainVm.headers
            }).success(function (response) {

                srCtrl.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    srCtrl.getConsignmentDetails(srCtrl.rhs_data.id);
                }


            });


        }
        else{
            /*if(srCtrl.selected_items[0].consignment_state != 'RCD'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }*/


            var sent_items = [];
            srCtrl.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            srCtrl.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/item/mark-sold-out',
                data : {
                    "consignment_id" : srCtrl.rhs_data.id,
                    "access_token" : $localStorage.fly_amazon.user_info.access_token,
                    "item_list" : sent_items,
                    "vendor_id" : srCtrl.selected_seller.id,
                    "marked_by_us" : true
                },
                headers: mainVm.headers
            }).success(function (response) {

                srCtrl.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    srCtrl.getConsignmentDetails(srCtrl.rhs_data.id);
                }


            });

        }

    }

    function openModalForImage(url) {
        srCtrl.imagePath = url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }

    function getVendorList() {

        srCtrl.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'vendor/',
            headers: mainVm.headers
        }).success(function (response) {

            srCtrl.vendor_list = [];
            srCtrl.spinners    = false;

            response.data.forEach(function (col) {

                if(col.type != 'WRHS'){
                    srCtrl.vendor_list.push(col);
                }

            });


        });

    }

}


angular
    .module('flyrobe')
    .controller('sellerReturnConsignmentCtrl', sellerReturnConsignmentCtrl);
