/**
 ********** Created by Surbhi Goel ************
 **/

function returnConsignmentListCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                         mainServices,ngDialog,DTOptionsBuilder) {

    rcCtrl = this;
    rcCtrl.panel = "RTD";
    rcCtrl.selected_items     = [];

    if(!$filter('tab')(rcCtrl.panel)) {
        $state.go('login');
    }


    mainVm.flyrobeState     = "Return Package";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    rcCtrl.consignment_status_list = {
        'INI' : [{'id' : 'RTS', 'name' : 'Ready to be shipped'}],
        'RTS' : [{'id' : 'ITR', 'name' : 'In Transit'}]
    };
    rcCtrl.selected_consignment = {};
    rcCtrl.gridOptionsComplex = {
        data : []
    };

    rcCtrl.getData                          = getData;
    rcCtrl.exportToCsv                      = exportToCsv;
    rcCtrl.getVendorList                    = getVendorList;
    rcCtrl.updateStatus                     = updateStatus;
    rcCtrl.viewDetails                      = viewDetails;
    rcCtrl.updatePackage                    = updatePackage;
    rcCtrl.updateConsignment                = updateConsignment;
    rcCtrl.updateStatusConsignment          = updateStatusConsignment;
    rcCtrl.selectItem                       = selectItem;

    rcCtrl.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withDisplayLength(50)
        .withOption('order', [1, 'asc'])
        .withButtons([
            {extend: 'copy'}
        ]);// dataTable Searching , Sorting & pagination

    rcCtrl.getVendorList(); //Default Call

    function getData(status,active_btn) {

        rcCtrl.spinners     = true;
        rcCtrl.active_status = active_btn;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'consignment/?type=RTD&status=' + status,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            rcCtrl.spinners = false;
            table_data = response.data;

            if(table_data.length){

                //column def
                {
                    var column = [];
                    rcCtrl.csv_header = [];

                    var col_name = {
                        name : 'checkbox',
                        displayName: '',
                        width : 40,
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p" ' +
                        'ng-click="grid.appScope.rcCtrl.selected_consignment = row.entity.full_info; grid.appScope.rcCtrl.cons.selected_status = \'\';grid.appScope.rcCtrl.active_row_index = row.entity.index">' +
                        '<label class="needsclick"> ' +
                        '<i class="fa fa-circle-o" ng-hide="grid.appScope.rcCtrl.selected_consignment.id == row.entity.full_info.id"></i> ' +
                        '<i class="fa fa-check-circle-o" ng-show="grid.appScope.rcCtrl.selected_consignment.id == row.entity.full_info.id"></i> ' +
                        '</label> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'id',
                        displayName: 'ID',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rcCtrl.csv_header.push('ID');

                    var col_name = {
                        name : 'created_at',
                        displayName: 'Created At',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rcCtrl.csv_header.push('Created At');

                    var col_name = {
                        name : 'created_by',
                        displayName: 'Created By',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rcCtrl.csv_header.push('Created By');

                    var col_name = {
                        name : 'status',
                        displayName: 'Status',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rcCtrl.csv_header.push('Status');

                    var col_name = {
                        name : 'waybill',
                        displayName: 'Waybill',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rcCtrl.csv_header.push('Waybill');

                    var col_name = {
                        name : 'shipped_on',
                        displayName: 'Shipped On',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rcCtrl.csv_header.push('Shipped On');

                    var col_name = {
                        name : 'source_vendor',
                        displayName: 'Source',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rcCtrl.csv_header.push('Source');

                    var col_name = {
                        name : 'destination_vendor',
                        displayName: 'Destination',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    rcCtrl.csv_header.push('Destination');

                    var col_name = {
                        name : 'items',
                        displayName: 'Items',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all">' +
                        '{{COL_FIELD}} ' +
                        '<a ng-click="grid.appScope.rcCtrl.viewDetails(row.entity.full_info)">' +
                        'View Details' +
                        '</a>' +
                        '</div>'
                    };
                    column.push(col_name);
                    rcCtrl.csv_header.push('Items');

                    /*var col_name = {
                     name : 'items',
                     displayName: 'Items',
                     enableCellEdit: false,
                     width : 250,
                     cellTemplate : '' +
                     '<div class="p-8 h-100p">' +
                     '{{COL_FIELD}}' +
                     '<table class="table table-striped table-bordered">' +
                     '<tr>' +
                     '<th>Item Id</th>' +
                     '<th>Item State</th>' +
                     '<th>Item Status</th>' +
                     '<th>Price</th>' +
                     '</tr>' +
                     '<tr ng-repeat="data in row.entity.full_info.items_info">' +
                     '<td>{{data.item_id}}</td>' +
                     '<td>{{data.item_state}}</td>' +
                     '<td>{{data.item_status}}</td>' +
                     '<td>{{data.price}}</td>' +
                     '</tr>' +
                     '</table>' +
                     '</div>'
                     };
                     column.push(col_name);*/

                }

                rcCtrl.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 45,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        rcCtrl.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){

                        if(row.entity.full_info.id == rcCtrl.selected_consignment.id){
                            return 'ui-grid-active-row';
                        }
                    },
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                table_data.forEach(function (res,$index) {

                    var temp = {
                        full_info : res,
                        id : res.id,
                        index : $index,
                        created_at : $filter('date')(new Date(res.created_at),'yyyy-MM-dd'),
                        created_by : res.created_by,
                        status : $filter('consignment_status')(res.status),
                        reserve_for_days : res.reserve_for_days,
                        waybill : res.waybill,
                        shipped_on : $filter('date')(new Date(res.shipped_on),'yyyy-MM-dd'),
                        source_vendor : (res.source_vendor == 0 ? 0 : rcCtrl.vendor_lsit[res.source_vendor]),
                        destination_vendor : (res.destination_vendor == 0 ? 0 : rcCtrl.vendor_lsit[res.destination_vendor]),
                        items : res.items_info.length
                    };

                    rcCtrl.gridOptionsComplex.data.push(temp);

                });

            }
            else{
                rcCtrl.gridOptionsComplex = {
                    data : []
                };
            }

        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function exportToCsv() {

        rcCtrl.csv_array = [];

        rcCtrl.gridApi.grid.rows.forEach(function (row) {
            if (row.visible) {

                var temp = {
                    id : row.entity.id,
                    created_at : row.entity.created_at,
                    created_by : row.entity.created_by,
                    status : row.entity.status,
                    waybill : row.entity.waybill,
                    shipped_on : row.entity.shipped_on,
                    source_vendor : row.entity.source_vendor,
                    destination_vendor : row.entity.destination_vendor,
                    items : row.entity.items
                };

                rcCtrl.csv_array.push(temp);

            }
        });

        return rcCtrl.csv_array;


    }

    function getVendorList() {

        rcCtrl.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'vendor/',
            headers: mainVm.headers
        }).success(function (response) {

            rcCtrl.vendor_lsit = {};

            response.data.forEach(function (col) {
                rcCtrl.vendor_lsit[col.id] = col.user_name;
            });

            rcCtrl.spinners     = false;
            rcCtrl.getData('INI,RTS,ITR,PRC','Active');

        });

    }

    function updatePackage(id) {
        rcCtrl.selected_items = [];
        if(!rcCtrl.selected_consignment.status){
          SweetAlert.swal({
              title: "",
              text: "Please select Consignment."
          });
        }else if(rcCtrl.selected_consignment.status == 'ITR' || rcCtrl.selected_consignment.status == 'PRC'){
          console.log(rcCtrl.selected_consignment.id)
          rcCtrl.spinners = true;
          if(id){
            $http({
                method: 'GET',
                url: mainVm.urlList.flymall + 'consignment?id=' + id,
                headers: mainVm.headers
            }).success(function (response) {
                rcCtrl.spinners = false;
                rcCtrl.rhs_data = response.data;
            });
          }else{
            $http({
                method: 'GET',
                url: mainVm.urlList.flymall + 'consignment?id=' + rcCtrl.selected_consignment.id,
                headers: mainVm.headers
            }).success(function (response) {
                rcCtrl.spinners = false;
                rcCtrl.rhs_data = response.data;
                rcCtrl.updateConsignment();
            });
          }
        }else{
          SweetAlert.swal({
              title: "",
              text: "Consignment should be In Transit or Partially Received"
          });
        }
    }

    function updateStatus() {

        if(rcCtrl.cons.selected_status == '' || rcCtrl.cons.selected_status == null || rcCtrl.cons.selected_status == undefined){
            SweetAlert.swal({
                title: "",
                text: "Please select Status."
            });
        }
        else if(rcCtrl.cons.selected_status == 'RTS' &&
            (rcCtrl.cons.logistic_partner == "" || rcCtrl.cons.logistic_partner == null || rcCtrl.cons.logistic_partner == undefined)){
            SweetAlert.swal({
                title: "",
                text: "Please select Logistics Partner."
            });
        }
        else{
            rcCtrl.spinners     = true;

            if(rcCtrl.cons.selected_status == 'RTS'){
                var send_data = {
                    "consignment_id" : rcCtrl.selected_consignment.id,
                    "to_status" : rcCtrl.cons.selected_status,
                    "logistic_partner" : rcCtrl.cons.logistic_partner
                };
            }
            else{
                var send_data = {
                    "consignment_id" : rcCtrl.selected_consignment.id,
                    "to_status" : rcCtrl.cons.selected_status
                };
            }

            $http({
                method: 'POST',
                url: mainVm.urlList.flymall + 'consignment/transition/moved/',
                data : send_data,
                headers: mainVm.headers
            }).success(function (response) {
                rcCtrl.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    rcCtrl.gridOptionsComplex.data[rcCtrl.active_row_index]['full_info']['status'] = rcCtrl.cons.selected_status;
                    rcCtrl.gridOptionsComplex.data[rcCtrl.active_row_index]['status'] = $filter('consignment_status')(rcCtrl.cons.selected_status);
                    rcCtrl.cons.selected_status = '';
                    rcCtrl.selected_consignment = {};
                    SweetAlert.swal({
                        title: "",
                        text: "Updated."
                    });
                }

            });


        }

    }

    function viewDetails(cons_details) {

        rcCtrl.cons_details = cons_details;

        ngDialog.open({
            template: 'viewDetails',
            className: 'ngdialog-theme-default consignment-view-modal',
            showClose: false,
            scope: $scope
        });

    }

    function updateConsignment() {

        ngDialog.open({
            template: 'updateConsignment',
            className: 'ngdialog-theme-default consignment-view-modal',
            showClose: true,
            scope: $scope
        });

    }

    function updateStatusConsignment(type) {

        if(rcCtrl.selected_items.length <= 0){
            SweetAlert.swal({
                title: "",
                text: "Please select atleast one item."
            });
            return false;
        }

        if(type == 'RCD'){
            /*if(rcCtrl.selected_items[0].consignment_state != 'SHP'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }*/

            var sent_items = [];
            rcCtrl.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            rcCtrl.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/consignment/received',
                data : {
                    "consignment_id" : rcCtrl.rhs_data.id,
                    "access_token" : $localStorage.fly_amazon.user_info.access_token,
                    "item_list" : sent_items,
                    "marked_by_us" : true
                },
                headers: mainVm.headers
            }).success(function (response) {

                rcCtrl.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    rcCtrl.updatePackage(rcCtrl.rhs_data.id);
                }


            });


        }
        else if(type == 'NRCD'){
            /*if(rcCtrl.selected_items[0].consignment_state != 'SHP'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }*/

            var sent_items = [];
            rcCtrl.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            rcCtrl.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/consignment/not-received',
                data : {
                    "consignment_id" : rcCtrl.rhs_data.id,
                    "access_token" : $localStorage.fly_amazon.user_info.access_token,
                    "item_list" : sent_items,
                    "marked_by_us" : true
                },
                headers: mainVm.headers
            }).success(function (response) {

                rcCtrl.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    rcCtrl.updatePackage(rcCtrl.rhs_data.id);
                }


            });


        }
        else{
            /*if(rcCtrl.selected_items[0].consignment_state != 'RCD'){
                SweetAlert.swal({
                    title: "",
                    text: "You have selected wrong items."
                });
                return false;
            }*/


            var sent_items = [];
            rcCtrl.selected_items.forEach(function (col) {
                sent_items.push(col.id);
            });

            rcCtrl.spinners     = true;
            $http({
                method: 'POST',
                url: mainVm.urlList.flymall2 + 'vendor/item/mark-sold-out',
                data : {
                    "consignment_id" : rcCtrl.rhs_data.id,
                    "access_token" : $localStorage.fly_amazon.user_info.access_token,
                    "item_list" : sent_items,
                    "vendor_id" : rcCtrl.rhs_data.destination_vendor,
                    "marked_by_us" : true
                },
                headers: mainVm.headers
            }).success(function (response) {

                rcCtrl.spinners     = false;

                if(response.is_error){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    rcCtrl.updatePackage(rcCtrl.rhs_data.id);
                }


            });

        }

    }

    function selectItem(item,type) {

        if(type == 'all'){

            if(rcCtrl.checked){
                rcCtrl.selected_items = [];
                item.forEach(function (col) {
                    col.checked = false;
                });
            }
            else{

                var common_status = true;

                item.forEach(function (col) {
                    if( col.status != 'SOT' && col.consignment_state != item[0].consignment_state){
                        common_status = false;
                    }
                });

                if(!common_status){
                    SweetAlert.swal({
                        title: "",
                        text: "Selected items must have common state."
                    });
                    return false;
                }
                else{
                    item.forEach(function (col) {
                        if(col.status != 'SOT'){
                            rcCtrl.selected_items.push(col);
                            col.checked = true;
                        }
                    });
                }
            }

            rcCtrl.checked = !rcCtrl.checked;

        }
        else{ //single selection
            if(item.checked){
                item.checked = false;

                var i = rcCtrl.selected_items.indexOf(item);
                if (i != -1) {
                    rcCtrl.selected_items.splice(i, 1);
                }

            }
            else{

                if(rcCtrl.selected_items.length > 0 && item.consignment_state != rcCtrl.selected_items[0].consignment_state){
                    SweetAlert.swal({
                        title: "",
                        text: "Selected items must have common state."
                    });
                    return false;
                }

                item.checked = true;

                var i = rcCtrl.selected_items.indexOf(item);
                if (i == -1) {
                    rcCtrl.selected_items.push(item);
                }
            }
        }//end of main else

    }

}


angular
    .module('flyrobe')
    .controller('returnConsignmentListCtrl', returnConsignmentListCtrl);
