/**
 ********** Created by Surbhi Goel ************
 **/

function loginCtrl($state,$timeout,$localStorage,$scope,$http,SweetAlert,$filter,commonService){

    login = this;
    login.account = {};
    login.spinners = false;


    if(!$localStorage.fly_amazon){
        $localStorage.fly_amazon = {};
    }


    login.loginToDashboard  = loginToDashboard; //function to login
    login.recover           = recover;

    /**********************************************
                function to login
     **********************************************/
    function loginToDashboard(){

        login.spinners = true;

            $http({
                method: 'POST',
                url: mainVm.urlList.order_url + "user-login/",
                data: {
                    "grant_type": 'password',
                    "username": login.account.email,
                    "password": login.account.password,
                    "resource": 'AMZ'
                }
            }).success(function (response) {

                login.spinners = false;

                if (response.is_error) {

                    if (response.status == 101) {
                        SweetAlert.swal({
                            title: "",
                            text: response.message
                        });
                        return false;
                    }

                }
                else {

                    if(response.data.user_role_based_access.length < 1 || response.data.user_role_based_access[0].resource_access_permissions.length < 1){
                        SweetAlert.swal({
                            title: "",
                            text: "Please enter valid username & password."
                        });
                        return false;
                    }


                    $localStorage.fly_amazon = response.data;
                    mainVm.fly_amazon = response.data;
                    mainVm.email = login.account.email;


                    $localStorage.fly_amazon.email = login.account.email;

                    mainVm.node_header = {
                        'Authorization': 'Bearer ' + mainVm.fly_amazon.user_info.access_token
                    };

                    mainVm.node_header_1 = {
                        'Authorization': 'Bearer ' + mainVm.fly_amazon.user_info.access_token,
                        'Content-Type': 'application/x-www-form-urlencoded'
                    };

                    mainVm.headers = {
                        'Authorization': 'Bearer ' + mainVm.fly_amazon.user_info.access_token,
                        'Content-Type': 'application/json'
                    };

                    $localStorage.fly_amazon.logs = {
                        "platform" : 'Amazon Dashboard',
                        "platform_properties" : {},
                        "user_email" : login.account.email,
                        "user_properties" : "",
                        "event" : "",
                        "event_properties" : {
                            'platform' : ""
                        },
                        "version" : mainVm.version
                    };

                    if($filter('tab')('MCH')){
                        $state.go('flyrobe.manufacture');
                    }
                    else if($filter('tab')('CAT')){
                        $state.go('flyrobe.catalog');
                    }
                    else if($filter('tab')('SEL')){
                        $state.go('flyrobe.vendor');
                    }
                    else if($filter('tab')('PICK')){
                        $state.go('flyrobe.picklist');
                    }
                    else if($filter('tab')('CONS')){
                        $state.go('flyrobe.consignment');
                    }
                    else if($filter('tab')('RTD')){
                        $state.go('flyrobe.returnConsignmentList');
                    }
                    else if($filter('tab')('TRUNK')){
                        $state.go('flyrobe.trunkshow');
                    }

                }


            }).error(function (response) {

            });



    }

    /**********************************************
     function to forgot pssword
     **********************************************/
    function recover() {

        login.forgotSpinners = true;

        $http({
            method: 'POST',
            url: mainVm.urlList.order_url + 'reset-password',
            data: {
                emails: [login.account.recoverEmail]
            },
            headers: mainVm.header1
        }).success(function (response, status, headers, config) {
            if (!response.is_error) {
                login.rSuccessMsg = "Please check the message sent to your registered email & follow the instructions to reset your password.";
                $timeout(function () {
                    $state.go('login');
                },4000);
            }

        }).error(function (response, status, headers, config) {

            console.log(response);
            console.log(status);

            if (status == 500) {
                SweetAlert.swal({
                    title: "",
                    text: "Something went wrong. Please try again after some time."
                });
                login.forgotSpinners = false;
            }
            else if (response.meta.is_error) {
                SweetAlert.swal({
                    title: "",
                    text: "This e-mail is not registered with us."
                });
                login.forgotSpinners = false;
            }
        });

    }

}


angular
    .module('flyrobe')
    .controller('loginCtrl', loginCtrl);
