/**
 ********** Created by Surbhi Goel ************
 **/

function itemListCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                     mainServices,ngDialog) {

    ilCtrl = this;
    ilCtrl.panel = "CAT";

    if(!$filter('tab')(ilCtrl.panel)) {
        $state.go('login');
    }


    ilCtrl.selected_items = [];
    ilCtrl.selected_manufacture = "";
    ilCtrl.status_array = [
        {id : 'AVL', name : 'Live'},
        {id : 'DMG', name : 'Damage'}
    ]


    mainVm.flyrobeState     = "Item List";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    ilCtrl.exportToCsv                      = exportToCsv;
    ilCtrl.getData                          = getData;
    ilCtrl.getManufacturerList              = getManufacturerList;
    ilCtrl.openModalForImage                = openModalForImage;
    ilCtrl.selectCheckBox                   = selectCheckBox;
    ilCtrl.updateStatus                     = updateStatus;

    ilCtrl.getManufacturerList(); // Default call

    function getData() {

        ilCtrl.spinners     = true;
        ilCtrl.selected_items = [];

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall2 + 'catalog/items/?manufacturer_id=' + ilCtrl.selected_manufacture,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            ilCtrl.spinners = false;
            table_data = response.data;

            if(table_data.length){

                //column def
                {
                    var column = [];
                    ilCtrl.csv_header = [];

                    var col_name = {
                        name : 'full_info',
                        displayName: '',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="checkbox c-checkbox needsclick p-8 h-100p"' +
                        'ng-click="grid.appScope.ilCtrl.selectCheckBox(COL_FIELD)"> ' +
                        '<label class="needsclick"> ' +
                        '<i class="fa fa-square-o" ng-hide="row.entity.full_info.checked"></i> ' +
                        '<i class="fa fa-check-square-o" ng-show="row.entity.full_info.checked"></i> ' +
                        '</label> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'id',
                        displayName: 'Id',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Id');

                    var col_name = {
                        name : 'identification_code',
                        displayName: 'Identification Code',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Identification Code');

                    var col_name = {
                        name : 'bin',
                        displayName: 'Bin Location',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Bin Location');

                    var col_name = {
                        name: 'image',
                        displayName: 'Image',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p"> ' +
                        '<img src="{{COL_FIELD}}" alt="Image" ng-click="grid.appScope.ilCtrl.openModalForImage(row.entity.image2)"' +
                        'class="img-responsive"' +
                        'style="height: 40px; width: 25px; cursor: pointer;"/> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'item_code',
                        displayName: 'Item Code',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Item Code');

                    var col_name = {
                        name : 'hsn',
                        displayName: 'HSN',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('HSN');

                    var col_name = {
                        name : 'category',
                        displayName: 'Category',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Category');

                    var col_name = {
                        name : 'cost',
                        displayName: 'Cost',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Cost');

                    var col_name = {
                        name : 'manufacturer',
                        displayName: 'Merchant',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Merchant');

                    var col_name = {
                        name : 'size',
                        displayName: 'Size',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Size');

                    var col_name = {
                        name : 'active_consignment',
                        displayName: 'Active Consignment',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Active Consignment');

                    var col_name = {
                        name : 'status',
                        displayName: 'Status',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Status');

                    var col_name = {
                        name : 'invoice_number',
                        displayName: 'Invoice Number',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Invoice Number');

                    var col_name = {
                        name : 'short_description',
                        displayName: 'Short Description',
                        enableCellEdit: false,
                        width : '100',
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    ilCtrl.csv_header.push('Short Description');


                }

                ilCtrl.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 70,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        ilCtrl.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){},
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                table_data.forEach(function (res) {

                    var temp = {
                        full_info : res,
                        id : res.id,
                        short_description : res.style_info.short_description,
                        identification_code : res.style_info.identification_code,
                        bin : '',
                        item_code : res.item_code,
                        invoice_number : res.invoice_number,
                        manufacturer : ilCtrl.manufacturer_list[res.manufacturer_id],
                        size : res.size,
                        active_consignment : res.current_consignment_id,
                        hsn : res.style_info.hsn,
                        category : res.style_info.category,
                        cost : res.style_info.cost,
                        image : mainVm.urlList.imagekit_100_url + res.style_info.image_front,
                        image2 : mainVm.urlList.imagekit_url + res.style_info.image_front,
                        status : $filter('itemStatus')(res.status)
                    };

                    if(res.style_info.image_front.indexOf('http') >= 0){
                        temp.image = res.style_info.image_front;
                        temp.image2 = res.style_info.image_front;
                    }

                    ilCtrl.gridOptionsComplex.data.push(temp);

                });

            }
            else{
                ilCtrl.gridOptionsComplex = {
                    data : []
                };
            }

        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function exportToCsv() {

        ilCtrl.csv_array = [];

        ilCtrl.gridApi.grid.rows.forEach(function (row) {
            if (row.visible) {

                var temp = {
                    id : row.entity.id,
                    identification_code : row.entity.identification_code,
                    bin : row.entity.bin,
                    item_code : row.entity.item_code,
                    hsn : row.entity.hsn,
                    category : row.entity.category,
                    cost : row.entity.cost,
                    manufacturer : row.entity.manufacturer,
                    size : row.entity.size,
                    active_consignment : row.entity.active_consignment,
                    status : row.entity.status,
                    invoice_number : row.entity.invoice_number,
                    short_description : row.entity.short_description
                };

                ilCtrl.csv_array.push(temp);

            }
        });

        return ilCtrl.csv_array;


    }

    function getManufacturerList() {

        ilCtrl.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'manufacturer/',
            headers: mainVm.headers
        }).success(function (response) {

            ilCtrl.manufacturer_list = {};
            ilCtrl.manufacturer_list_2 = response.data;
            response.data.forEach(function (col) {
                ilCtrl.manufacturer_list[col.id] = col.user_name;
            });


            ilCtrl.getData(); // Default call

        });

    }

    function updateStatus() {

        if(!ilCtrl.selected_status){
            SweetAlert.swal({
                title: "",
                text: "Please select Status."
            });
            return false;
        }

        if(ilCtrl.selected_items.length == 0){
            SweetAlert.swal({
                title: "",
                text: "Please select atleast one item."
            });
            return false;
        }

        ilCtrl.spinners     = true;


        $http({
            method: 'PUT',
            url: mainVm.urlList.flymall2 + 'catalog/items/status/',
            data : {
                "ids" : ilCtrl.selected_items,
                "to_status" : ilCtrl.selected_status
            },
            headers: mainVm.headers
        }).success(function (response) {

            ilCtrl.spinners     = false;

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: response.message
                });
            }
            else{
                ilCtrl.selected_status = "";
                ilCtrl.selected_items = [];
                SweetAlert.swal({
                    title: "",
                    text: "Updated."
                },function () {
                    $state.reload();
                });
            }


        });

    }

    function selectCheckBox(col) {

        if(col.checked){
            col.checked = false;

            var i = ilCtrl.selected_items.indexOf(col.id);

            if (i != -1) {
                ilCtrl.selected_items.splice(i, 1);
            }

        }
        else{
            col.checked = true;

            var i = ilCtrl.selected_items.indexOf(col.id);

            if (i == -1) {
                ilCtrl.selected_items.push(col.id);
            }
        }

    }

    function openModalForImage(url) {
        ilCtrl.imagePath = url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }

}


angular
    .module('flyrobe')
    .controller('itemListCtrl', itemListCtrl);
