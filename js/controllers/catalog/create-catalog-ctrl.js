/**
 ********** Created by Surbhi Goel ************
 **/

function createCatalogCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                               mainServices) {

    ccCtrl = this;
    ccCtrl.panel = "CAT";

    if(!$filter('tab')(ccCtrl.panel)) {
        $state.go('login');
    }


    ccCtrl.spinner = true;


    ccCtrl.catagory_list = [
      'Stitched','Semi-stitched','Unstitched'
    ];
    ccCtrl.size_qty = {};
    ccCtrl.catalog  = {
        gender : 'FML'
    };
    ccCtrl.upload_files_type = '';

    ccCtrl.female_size_list = ['32','34','36','38','40','42','44','46','48'];
    ccCtrl.male_size_list   = ['28','30','32','34','36','38','40','42','44','46','48','50','52','54','56'];


    mainVm.flyrobeState     = "Create Catalog";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    ccCtrl.changeCategory               = changeCategory;
    ccCtrl.createCatalog                = createCatalog;
    // ccCtrl.createSizeQtyArr             = createSizeQtyArr;
    ccCtrl.getFillerDetails             = getFillerDetails;
    ccCtrl.getManufactureList           = getManufactureList;
    ccCtrl.uploadFile                   = uploadFile;
    ccCtrl.uploadFileOnServer           = uploadFileOnServer;
    ccCtrl.uploadInvoiceOnServer        = uploadInvoiceOnServer;
    ccCtrl.getTagList                   = getTagList;

    ccCtrl.getManufactureList(); //Default Call
    ccCtrl.getTagList();

    function changeCategory() {

        ccCtrl.catalog.size = [];
        ccCtrl.size_qty = {}

        if(ccCtrl.catalog.category == 'Stitched'){
            ccCtrl.female_size_list = ['32','34','36','38','40','42','44','46','48']
        }
        else if(ccCtrl.catalog.category == 'Semi-stitched'){
            ccCtrl.female_size_list = ['32-43']
        }
        else if(ccCtrl.catalog.category == 'Unstitched'){
            ccCtrl.female_size_list = ['FS']
        }

    }

    function createCatalog() {

      if(!ccCtrl.catalog.tags || !ccCtrl.catalog.tags.id){
          SweetAlert.swal({
              title: "",
              text: "Please select a Style Category."
          });
          ccCtrl.spinners = false;
          return false;
      }
      
        ccCtrl.spinners     = true;
        var send_data = {
            "manufacturer_id": ccCtrl.catalog.manufacture.id,
            "vendor_sku_code": ccCtrl.catalog.vendor_sku_code,
            "gender": ccCtrl.catalog.gender,
            "is_filler": (ccCtrl.catalog.filler == true || ccCtrl.catalog.filler == 'true' ? true : false),
            "identification_code": ccCtrl.catalog.identification_code,
            "short_description": ccCtrl.catalog.short_desc,
            "invoice_number": ccCtrl.catalog.invoice_number,
            "long_description": ccCtrl.catalog.long_desc,
            "fitting_notes": ccCtrl.catalog.fitting_notes,
            "category": (ccCtrl.catalog.category ? ccCtrl.catalog.category : ''),
            "tags" : [ccCtrl.catalog.tags.id],
            "no_of_outfits": 0,
            "mrp": 0,
            "cost_currency": "INR",
            "cost": (ccCtrl.catalog.cost ? parseInt(ccCtrl.catalog.cost) : ''),
            "brand": "",
            "whats_included": ccCtrl.catalog.whats_included,
            "hsn": (ccCtrl.catalog.hsn ? ccCtrl.catalog.hsn : ''),
            // "sac": (ccCtrl.catalog.sac ? ccCtrl.catalog.sac : ''),
            "gst_percentage": (ccCtrl.catalog.gst_percentage ? ccCtrl.catalog.gst_percentage : ''),
            "image_front": (ccCtrl.front_image ? ccCtrl.front_image : ''),
            "image_back": (ccCtrl.back_image ? ccCtrl.back_image : ''),
            "image_multi": (ccCtrl.multi_image ? ccCtrl.multi_image : []),
            "invoice_link" : (ccCtrl.invoice_image ? ccCtrl.invoice_image : ''),
            "sizes": []
        };

        if(!ccCtrl.catalog.manufacture || !ccCtrl.catalog.manufacture.id){
            SweetAlert.swal({
                title: "",
                text: "Please select Merchant."
            });
            ccCtrl.spinners = false;
            return false;
        }

        if(!ccCtrl.catalog.size || ccCtrl.catalog.size.length == 0){
            SweetAlert.swal({
                title: "",
                text: "Please select size."
            });
            ccCtrl.spinners = false;
            return false;
        }

        ccCtrl.catalog.size.forEach(function (size) {
            if(ccCtrl.size_qty[size]){
                for(var i = 0;i < ccCtrl.size_qty[size]; i++){
                    send_data.sizes.push(size);
                }
            }
        });

        send_data.no_of_outfits = send_data.sizes.length;

        $http({
            method: 'POST',
            url: mainVm.urlList.flymall + 'catalog/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            ccCtrl.spinners = false;

            if(response.is_error){

                if(typeof(response.message) == 'string'){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message[0]
                    });
                }

            }
            else{
                ccCtrl.manufacture = {};
                SweetAlert.swal({
                    title: "",
                    text: "Created"
                },function () {
                    $state.go('flyrobe.catalog');
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function getManufactureList() {

        ccCtrl.spinner = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'manufacturer/',
            headers: mainVm.headers
        }).success(function (response) {
            ccCtrl.manufacture_list = response.data;
            ccCtrl.spinner = false;
        });

    }

    function getTagList() {

        ccCtrl.spinner = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall2 + 'catalog/tags',
            headers: mainVm.headers
        }).success(function (response) {
            ccCtrl.tags_list = response.data;
            ccCtrl.spinner = false;
        });

    }

    function uploadFile(type,input) {

        if ((type == 'front_image' || type == 'back_image' || type == 'invoice_image') && input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#' + type)
                    .attr('src', e.target.result)
                    .width(50)
                    .height(34);
            };

            ccCtrl[type] = input.files[0];
            reader.readAsDataURL(input.files[0]);

            if(type == 'front_image'){
                $('.' + type + '-div input').removeClass('parsley-error');
                $('.' + type + '-div ul').css('display','none');
                ccCtrl.upload_files_type += "F";
            }
            else if(type == 'back_image'){
                ccCtrl.upload_files_type += "B";
            }

        }
        else{
            ccCtrl.upload_files_type += "M";
            ccCtrl[type] = input.files;
        }

    }

    function uploadInvoiceOnServer() {

        if(ccCtrl.invoice_image){

            ccCtrl.spinners     = true;

            // ccCtrl.invoice_image = "https://s3-ap-southeast-1.amazonaws.com/flyrobe.user.logs/parent_item_dummy_images/28-Nov-2018-16-16-87";

            if(typeof(ccCtrl.invoice_image) == 'string' && ccCtrl.invoice_image.indexOf('http') >= 0){ //Already Uploaded on server
                ccCtrl.uploadFileOnServer();
            }
            else{

                var formData = new FormData();
                formData.append('dummy_image', ccCtrl.invoice_image);

                $.ajax({
                    type: 'POST',
                    url: mainVm.urlList.order_url + 'file-upload-to-s3',
                    dataType: "json",
                    data: formData,
                    async: true,
                    processData: false,
                    contentType: false,
                    success: function (data) {

                        ccCtrl.invoice_image = data.data;
                        ccCtrl.uploadFileOnServer();

                        $scope.$apply();

                    },
                    error: function (data) {
                        mainServices.checkErrorStatus(data.status);
                    }
                });

            }

        }
        else{ //no-invoice
            ccCtrl.uploadFileOnServer();
        }

    }

    function uploadFileOnServer() {

        ccCtrl.spinners     = true;

        // ccCtrl.front_image = "https://s3-ap-southeast-1.amazonaws.com/flyrobe.user.logs/parent_item_dummy_images/28-Nov-2018-16-16-87";

        // if(typeof(ccCtrl.front_image) == 'string' && ccCtrl.front_image.indexOf('http') >= 0){ //Already Uploaded on server
        if(ccCtrl.upload_files_type.length == 0){ //Already Uploaded on server
            ccCtrl.createCatalog();
        }
        else{

            var formData = new FormData();
            var multi_count = 0;
            var count = 0;

            if(ccCtrl.upload_files_type.indexOf('F') >= 0){
                formData.append('multi_img_' + count, ccCtrl.front_image);
                multi_count += 1;
                count += 1;
            }
            if(ccCtrl.upload_files_type.indexOf('B') >= 0){
                formData.append('multi_img_' + count, ccCtrl.back_image);
                multi_count += 1;
                count += 1;
            }
            if(ccCtrl.upload_files_type.indexOf('M') >= 0){

                for(var i = 0; i < ccCtrl.multi_image.length; i++){
                    formData.append('multi_img_' + count, ccCtrl.multi_image[i]);
                    multi_count += 1;
                    count += 1;
                }

            }

            formData.append('multi_count', multi_count);

            $.ajax({
                type: 'POST',
                // url: mainVm.urlList.order_url + 'file-upload-to-s3',
                url: mainVm.urlList.order_url + 'image-upload-to-cloudinary',
                dataType: "json",
                data: formData,
                async: true,
                processData: false,
                contentType: false,
                success: function (data) {

                    // ccCtrl.front_image = data.data;
                    // ccCtrl.spinner = false;

                    //Front Image
                    if(ccCtrl.upload_files_type.indexOf('F') >= 0){
                        ccCtrl.front_image = data.data[0];
                    }

                    //Back Image
                    if(ccCtrl.upload_files_type.indexOf('F') >= 0 && ccCtrl.upload_files_type.indexOf('B') >= 0){
                        ccCtrl.back_image = data.data[1];
                    }
                    else if(ccCtrl.upload_files_type.indexOf('B') >= 0){
                        ccCtrl.back_image = data.data[0];
                    }

                    //Multi Image
                    if(ccCtrl.upload_files_type.indexOf('F') >= 0 && ccCtrl.upload_files_type.indexOf('B') >= 0
                        && ccCtrl.upload_files_type.indexOf('M') >= 0){
                        ccCtrl.multi_image = [];
                        var len = data.data.length;

                        for(var i = 2; i< len; i++){
                            ccCtrl.multi_image.push(data.data[i]);
                        }

                    }
                    else if(ccCtrl.upload_files_type.indexOf('B') >= 0 && ccCtrl.upload_files_type.indexOf('M') >= 0){
                        ccCtrl.multi_image = [];
                        var len = data.data.length;

                        for(var i = 1; i< len; i++){
                            ccCtrl.multi_image.push(data.data[i]);
                        }

                    }
                    else if(ccCtrl.upload_files_type.indexOf('M') >= 0){
                        ccCtrl.multi_image = data.data;
                    }

                    ccCtrl.upload_files_type = "";

                    ccCtrl.createCatalog();
                    $scope.$apply();

                },
                error: function (data) {
                    mainServices.checkErrorStatus(data.status);
                }
            });

        }

    }

    function getFillerDetails() {

        if(!ccCtrl.catalog.identification_code){
            SweetAlert.swal({
                title: "",
                text: "Please enter Identification Code"
            });
            return false;
        }

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'catalog/?identification_code=' + ccCtrl.catalog.identification_code,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            ccCtrl.spinner = true;
            console.log("response ",response);

            if(response.data.length > 0){
                console.log("response ",response);
                ccCtrl.catalog.filler = 'true';
                ccCtrl.catalog.whats_included = response.data[0].whats_included;
                ccCtrl.catalog.category = response.data[0].category;
                ccCtrl.catalog.short_desc = response.data[0].short_description;
                ccCtrl.catalog.long_desc = response.data[0].long_description;
                ccCtrl.catalog.fitting_notes = response.data[0].fitting_notes;
                ccCtrl.catalog.cost = response.data[0].cost;
                ccCtrl.catalog.hsn = response.data[0].hsn;
                ccCtrl.catalog.gst_percentage = response.data[0].gst_percentage;

                if(response.data[0].image_front){

                    if(response.data[0].image_front.indexOf('http') >= 0){

                        ccCtrl.front_image = response.data[0].image_front;
                        $('#front_image')
                            .attr('src', response.data[0].image_front)
                            .width(50)
                            .height(34);

                    }
                    else{

                        ccCtrl.front_image = mainVm.urlList.imagekit_url + response.data[0].image_front;
                        $('#front_image')
                            .attr('src', mainVm.urlList.imagekit_url + response.data[0].image_front)
                            .width(50)
                            .height(34);

                    }


                }

                if(response.data[0].image_back){

                    if(response.data[0].image_back.index('http') >= 0){
                        ccCtrl.back_image = response.data[0].image_back;
                        $('#back_image')
                            .attr('src', response.data[0].image_back)
                            .width(50)
                            .height(34);
                    }
                    else{
                        ccCtrl.back_image = mainVm.urlList.imagekit_url + response.data[0].image_back;
                        $('#back_image')
                            .attr('src', mainVm.urlList.imagekit_url + response.data[0].image_back)
                            .width(50)
                            .height(34);
                    }

                }

                if(response.data[0].image_multi.length > 0){
                    ccCtrl.multi_image = response.data[0].image_multi;
                }

                ccCtrl.changeCategory();


            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Identification Code don't exist"
                });
            }

        });

    }

    // function createSizeQtyArr() {
    //     ccCtrl.catalog.size.forEach(function (col) {
    //         if(!ccCtrl.size_qty[col]){
    //             // ccCtrl.size_qty[col] = 0;
    //         }
    //     });
    // }

}


angular
    .module('flyrobe')
    .controller('createCatalogCtrl', createCatalogCtrl);
