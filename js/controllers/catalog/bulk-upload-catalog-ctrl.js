/**
 ********** Created by Surbhi Goel ************
 **/

function bulkcatalogCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                     ngDialog,DTOptionsBuilder) {

    bk = this;
    bk.panel = "CAT";

    if(!$filter('tab')(bk.panel)) {
        $state.go('login');
    }

    bk.table_data = [];

    mainVm.flyrobeState     = "Bulk Upload Catalog";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;


    bk.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withDisplayLength(50)
        .withOption('order', [2, 'asc'])
        .withButtons([
            {extend: 'copy'}
        ]);// dataTable Searching , Sorting & pagination


    bk.getCSVFormat                     = getCSVFormat;
    bk.openModalForImage                = openModalForImage;
    bk.readCSVFile                      = readCSVFile;


    bk.getCSVFormat(); //Default Call
    
    function getCSVFormat() {

        bk.csvArray = [{}];

        bk.getHeader = [
            "Merchant ID",
            "Merchant Name",
            "Host SKU Code",
            "Gender",
            "Identification Code",
            "Cost",
            "HSN",
            "GST(%)",
            "Front-Image(URL)",
            "Short Description",
            "Long Description",
            "Fitting Notes",
            "Category",
            "SAC",
            "Size",
            "Quantity",
            "Filler(For Male)"
        ];
        
    }
    

    function openModalForImage(url) {
        bk.imagePath = url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }

    function readCSVFile(filename) {

        bk.table_data = [];

        var file = filename.files[0];
        var fileSize = 0;
        if (filename.files[0]) {

            var reader = new FileReader();
            reader.onload = function (e) {
                var table = $("<table />").css('width','100%');

                var rows = e.target.result.split("\n");

               /* for (var i = 0; i < rows.length; i++) {
                    var row = $("<tr  />");
                    var cells = rows[i].split(",");
                    for (var j = 0; j < cells.length; j++) {
                        var cell = $("<td />").css('border','1px solid black');
                        cell.html(cells[j]);
                        row.append(cell);
                    }
                    table.append(row);
                }*/

                for (var i = 1; i < rows.length; i++) {

                    var cells = rows[i].split(",");

                    var temp = {
                        "manufacturer_id" : cells[0],
                        "manufacture_name" : cells[1],
                        "vendor_sku_code" : cells[2],
                        "gender" : cells[3],
                        "price" : cells[4],
                        "cost" : cells[5],
                        "hsn" : cells[6],
                        "gst_percentage" : cells[7],
                        "image_front" : cells[8],
                        "short_description" : cells[9],
                        "long_description" : cells[10],
                        "fitting_notes" : cells[11],
                        "category" : cells[12],
                        "sac" : cells[13],
                        "sizes" : cells[14],
                        "no_of_outfits" : cells[15],
                        "filler" : cells[16]
                    };

                    bk.table_data.push(temp);
                }

                // $("#dvCSV").html('');
                // $("#dvCSV").append(table);
                $scope.$apply();
            };

            reader.readAsText(filename.files[0]);

        }


    }

}


angular
    .module('flyrobe')
    .controller('bulkcatalogCtrl', bulkcatalogCtrl);