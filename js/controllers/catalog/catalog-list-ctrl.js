/**
 ********** Created by Surbhi Goel ************
 **/

function catalogCtrl($scope,$http,$filter,$state,$localStorage,SweetAlert,commonService,
                         mainServices,ngDialog,DTOptionsBuilder) {

    cc = this;
    cc.panel = "CAT";
    if(!$filter('tab')(cc.panel)) {
        $state.go('login');
    }

    cc.status_array = [
        {id : 'AVL', name : 'Live'},
        {id : 'DMG', name : 'Damage'}
    ];

    cc.item_status = [
        {id : 'PI', name : 'Initial'},
        {id : 'AVL', name : 'Live'},
        {id : 'STS', name : 'Sent To Host'},
        {id : 'DMG', name : 'Damaged'},
        {id : 'INT', name : 'In transit'},
        {id : 'SOT', name : 'Sold Out'}
    ];

    cc.selected_catalog = {
        id : ''
    };

    cc.female_size_list = ['32','34','36','38','40','42','44','46','48'];

    mainVm.flyrobeState     = "Catalog";
    $localStorage.fly_amazon.state = mainVm.flyrobeState;

    cc.exportToCsv                      = exportToCsv;
    cc.getData                          = getData;
    cc.getManufacturerList              = getManufacturerList;
    cc.openEditCatalogModal             = openEditCatalogModal;
    cc.openModalForImage                = openModalForImage;
    cc.openUpdateSizeModal              = openUpdateSizeModal;
    cc.selectItem                       = selectItem;
    cc.UpdateCatalogDetails             = UpdateCatalogDetails;
    cc.uploadFile                       = uploadFile;
    cc.uploadFilesOnServer              = uploadFilesOnServer;
    cc.updateStatus                     = updateStatus;
    cc.updateItemSize                   = updateItemSize;
    cc.viewDetails                      = viewDetails;
    cc.getTagList                       = getTagList;
    cc.tagUpdate                        = tagUpdate;

    cc.getManufacturerList(); // Default call
    cc.getTagList();


    cc.dtOptions = DTOptionsBuilder.newOptions()
        .withDOM('<"html5buttons"B>lTfgitp')
        .withDisplayLength(50)
        .withOption('order', [1, 'asc'])
        .withButtons([
            {extend: 'copy'}
        ]);// dataTable Searching , Sorting & pagination

    function getData() {

        cc.spinners     = true;

        if(cc.selected_manufacture){
            var url = mainVm.urlList.flymall + 'catalog/?manufacturer_id=' + cc.selected_manufacture;
        }
        else{
            var url = mainVm.urlList.flymall + 'catalog/?'
        }

        if(cc.selected_status){
            url += '&status=' + cc.selected_status;
        }

        $http({
            method: 'GET',
            url: url,
            headers: mainVm.headers
        }).success(function (response) {

            cc.breadcramb = {
                status : cc.selected_status,
                manufacture : cc.selected_manufacture
            };
            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            cc.spinners = false;
            table_data = response.data;

            if(table_data.length){

                //column def
                {
                    var column = [];
                    cc.csv_header = [];

                    var col_name = {
                        name : 'checkbox',
                        displayName: '',
                        width : 40,
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p word-break-all" ' +
                        'ng-click="grid.appScope.cc.selected_catalog = row.entity;grid.appScope.cc.selected_index = row.entity.index">' +
                        '<label class="needsclick"> ' +
                        '<i class="fa fa-circle-o" ng-hide="grid.appScope.cc.selected_catalog.id == row.entity.id"></i> ' +
                        '<i class="fa fa-check-circle-o" ng-show="grid.appScope.cc.selected_catalog.id == row.entity.id"></i> ' +
                        '</label> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'created_at',
                        displayName: 'Created At',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    cc.csv_header.push('Created At');

                    var col_name = {
                        name : 'identification_code',
                        displayName: 'Iden. Code',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    cc.csv_header.push('Identification Code');

                    var col_name = {
                        name : 'vendor_sku_code',
                        displayName: 'Host SKU Code',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    cc.csv_header.push('Host SKU Code');

                    var col_name = {
                        name: 'image',
                        displayName: 'images',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p"> ' +
                        '<img src="{{COL_FIELD}}" alt="Image" ng-click="grid.appScope.cc.openModalForImage(row.entity.image2)"' +
                        'class="img-responsive img-circle"' +
                        'style="height: 40px; width: 25px; cursor: pointer;"/> ' +
                        '</div>'
                    };
                    column.push(col_name);

                    var col_name = {
                        name : 'category',
                        displayName: 'Category',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    cc.csv_header.push('Category');

                    var col_name = {
                        name : 'hsn',
                        displayName: 'HSN',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    cc.csv_header.push('HSN');

                    var col_name = {
                        name : 'invoice_number',
                        displayName: 'Invoice Number',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    cc.csv_header.push('Invoice Number');

                    var col_name = {
                        name : 'cost',
                        displayName: 'Cost',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}}' +
                        '</div>'
                    };
                    column.push(col_name);
                    cc.csv_header.push('Cost');

                    var col_name = {
                        name : 'no_of_outfits',
                        displayName: 'QTY',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}} ' +
                        '<a ng-click="grid.appScope.cc.viewDetails(row.entity.full_info)">' +
                        ' View Details' +
                        '</a>' +
                        '</div>'
                    };
                    column.push(col_name);
                    cc.csv_header.push('QTY');

                    var col_name = {
                        name : 'whats_included',
                        displayName: 'Whats Included',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}} ' +
                        '</div>'
                    };
                    column.push(col_name);
                    cc.csv_header.push('Whats Included');

                    var col_name = {
                        name : 'short_description',
                        displayName: 'Short Description',
                        enableCellEdit: false,
                        cellTemplate : '' +
                        '<div class="p-8 h-100p">' +
                        '{{COL_FIELD}} ' +
                        '</div>'
                    };
                    column.push(col_name);
                    cc.csv_header.push('Short Description');

                }

                cc.gridOptionsComplex = {
                    enableFiltering: true,
                    showGridFooter: true,
                    showColumnFooter: true,
                    paginationPageSizes: [10, 25, 50,100],
                    paginationPageSize: 100,
                    rowHeight: 70,
                    columnDefs: column,
                    exporterCsvFilename: 'myFile.csv',
                    exporterPdfDefaultStyle: {fontSize: 9},
                    exporterPdfTableStyle: {margin: [30, 30, 30, 30]},
                    exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
                    exporterPdfHeader: { text: "My Header", style: 'headerStyle' },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                        return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                        docDefinition.styles.headerStyle = { fontSize: 22, bold: true };
                        docDefinition.styles.footerStyle = { fontSize: 10, bold: true };
                        return docDefinition;
                    },
                    exporterPdfOrientation: 'portrait',
                    exporterPdfPageSize: 'LETTER',
                    exporterPdfMaxGridWidth: 500,
                    exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
                    onRegisterApi: function(gridApi){
                        // $scope.gridApi = gridApi;
                        cc.gridApi = gridApi;
                    },
                    data: [],
                    rowStyle: function(row){

                        if(row.entity.id == cc.selected_catalog.id){
                            return 'ui-grid-active-row';
                        }

                    },
                    rowTemplate : '<div ng-class="grid.options.rowStyle(row)"> ' +
                    '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns" ' +
                    'ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader}" class="ui-grid-cell"' +
                    'role="{{col.isRowHeader ? \'rowheader\' : \'gridcell\'}}" ui-grid-cell> ' +
                    '</div>' +
                    '</div>'
                };

                //data
                table_data.forEach(function (res,$index) {

                    var temp = {
                        full_info : res,
                        index : $index,
                        id : res.id,
                        created_at : $filter('date')(new Date(res.created_at),'dd-MMM-yyyy'),
                        identification_code : res.identification_code,
                        vendor_sku_code : res.vendor_sku_code,
                        image : mainVm.urlList.imagekit_100_url + res.image_front,
                        image2 : mainVm.urlList.imagekit_url + res.image_front,
                        category : res.category,
                        hsn : res.hsn,
                        invoice_number : res.invoice_number,
                        cost : res.cost,
                        no_of_outfits : res.no_of_outfits,
                        whats_included : res.whats_included,
                        short_description : res.short_description
                    };

                    if(res.image_front.indexOf('http') >= 0){
                        temp.image = res.image_front;
                        temp.image2 = res.image_front;
                    }

                    cc.gridOptionsComplex.data.push(temp);

                });

            }
            else{
                cc.gridOptionsComplex = {
                    data : []
                };
            }

        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });

    }

    function exportToCsv() {

        cc.csv_array = [];

        cc.gridApi.grid.rows.forEach(function (row) {
            if (row.visible) {

                var temp = {
                    created_at : row.entity.created_at,
                    identification_code : row.entity.identification_code,
                    vendor_sku_code : row.entity.vendor_sku_code,
                    category : row.entity.category,
                    hsn : row.entity.hsn,
                    invoice_number : row.entity.invoice_number,
                    cost : row.entity.cost,
                    no_of_outfits : row.entity.no_of_outfits,
                    whats_included : row.entity.whats_included,
                    short_description : row.entity.short_description
                };

                cc.csv_array.push(temp);

            }
        });

        return cc.csv_array;

    }

    function openModalForImage(url) {
        cc.imagePath = url;

        ngDialog.open({
            template: 'imageModals',
            className: 'ngdialog-theme-default',
            showClose: false,
            scope: $scope
        });

    }

    function viewDetails(catalog_data) {

        cc.spinners     = true;
        cc.selected_items = [];

        cc.selected_catalog = catalog_data;

        if(cc.breadcramb.status){
            var url = mainVm.urlList.flymall2 + 'catalog/items/?style_details_id=' + catalog_data.id + '&status=' + cc.breadcramb.status;
        }
        else{
            var url = mainVm.urlList.flymall2 + 'catalog/items/?style_details_id=' + catalog_data.id;
        }

        $http({
            method: 'GET',
            url: url,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            cc.selected_catalog.item_info = response.data;
            cc.spinners     = false;

            ngDialog.open({
                template: 'viewDetails',
                className: 'ngdialog-theme-default consignment-view-modal',
                showClose: true,
                scope: $scope
            });


        });

    }

    function selectItem(col) {

        if(col.checked){
            col.checked = false;

            var i = cc.selected_items.indexOf(col.id);

            if (i != -1) {
                cc.selected_items.splice(i, 1);
            }

        }
        else{
            col.checked = true;

            var i = cc.selected_items.indexOf(col.id);

            if (i == -1) {
                cc.selected_items.push(col.id);
            }
        }

    }

    function updateStatus() {

        if(!cc.selected_status){
            SweetAlert.swal({
                title: "",
                text: "Please select Status."
            });
            return false;
        }

        if(cc.selected_items.length == 0){
            SweetAlert.swal({
                title: "",
                text: "Please select atleast one item."
            });
            return false;
        }

        cc.spinners     = true;

        ngDialog.close({
            template: 'viewDetails',
            className: 'ngdialog-theme-default consignment-view-modal',
            showClose: true,
            scope: $scope
        });


        $http({
            method: 'PUT',
            url: mainVm.urlList.flymall2 + 'catalog/items/status/',
            data : {
                "ids" : cc.selected_items,
                "to_status" : cc.selected_status
            },
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            cc.spinners     = false;

            if(response.is_error){
                SweetAlert.swal({
                    title: "",
                    text: response.message
                });
            }
            else{
                cc.selected_status = "";
                cc.selected_items = [];
                SweetAlert.swal({
                    title: "",
                    text: "Updated."
                },function () {
                    $state.reload();
                });
            }


        });

    }

    function getManufacturerList() {

        cc.spinners     = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall + 'manufacturer/',
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            cc.manufacturer_list = {};
            cc.manufacturer_list_2 = response.data;
            response.data.forEach(function (col) {
                cc.manufacturer_list[col.id] = col.user_name;
            });

            cc.spinners     = false;

        });

    }

    function openEditCatalogModal() {

        if(cc.selected_catalog.id){

            //front-image
            if(cc.selected_catalog.full_info.image_front.indexOf('http') < 0){
                cc.selected_catalog.new_front_image_url = mainVm.urlList.imagekit_120_url + cc.selected_catalog.full_info.image_front;
            }
            else{
                cc.selected_catalog.new_front_image_url = cc.selected_catalog.full_info.image_front;
            }

            //Back image
            if(cc.selected_catalog.full_info.image_back && cc.selected_catalog.full_info.image_back.indexOf('http') < 0){
                cc.selected_catalog.new_back_image = mainVm.urlList.imagekit_120_url + cc.selected_catalog.full_info.image_back;
            }
            else{

                cc.selected_catalog.new_back_image = cc.selected_catalog.full_info.image_back;
            }

            //Multi image
            if(cc.selected_catalog.full_info.image_multi && cc.selected_catalog.full_info.image_multi.length > 0
                && cc.selected_catalog.full_info.image_multi.indexOf('http') < 0){
                cc.selected_catalog.new_multi_image = mainVm.urlList.imagekit_120_url + cc.selected_catalog.full_info.image_multi;
            }
            else{

                cc.selected_catalog.new_multi_image = cc.selected_catalog.full_info.image_multi;
            }

            cc.upload_files_type = "";
            if(cc.selected_catalog.full_info.tags){
              cc.select_tags = []
              for(var i=0; i<cc.tags_list.length; i++){
                cc.selected_catalog.full_info.tags.map((item) => {
                  if(item == cc.tags_list[i].id){
                    cc.select_tags.push(cc.tags_list[i]);
                  }
                })
              }
              cc.tagUpdate();
            }else{
              cc.selected_catalog.full_info.tags = []
              cc.select_tags = []
              cc.tagUpdate();
            }



            ngDialog.open({
                template: 'updateCatalogDetailsModal',
                className: 'ngdialog-theme-default',
                showClose: true,
                scope: $scope
            });
        }
        else{
            SweetAlert.swal({
                title: "",
                text: "Please select Catalog."
            });
        }

    }

    function getTagList() {

        cc.spinner = true;

        $http({
            method: 'GET',
            url: mainVm.urlList.flymall2 + 'catalog/tags',
            headers: mainVm.headers
        }).success(function (response) {
            cc.tags_list = response.data;
            cc.spinner = false;
        });

    }

    function tagUpdate() {

      cc.tag_id = [];
      if(cc.select_tags){
        cc.select_tags.map((item) => {
          cc.tag_id.push(item.id)
        })
      }

    }

    function UpdateCatalogDetails() {

        cc.spinners     = true;

        ngDialog.close({
            template: 'updateCatalogDetailsModal',
            className: 'ngdialog-theme-default',
            showClose: true,
            scope: $scope
        });

        cc.selected_catalog.full_info.tags = cc.tag_id;
        var send_data = {
            "id": cc.selected_catalog.id,
            "short_description": cc.selected_catalog.short_description,
            "whats_included": cc.selected_catalog.whats_included,
            "image_front": cc.selected_catalog.send_front_image,
            "tags": cc.tag_id,
            "cost":parseInt(cc.selected_catalog.cost)
        };

        $http({
            method: 'PUT',
            url: mainVm.urlList.flymall + 'catalog/style/',
            data : send_data,
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            cc.spinners = false;

            if(response.is_error){

                if(typeof(response.message) == 'string'){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message[0]
                    });
                }

            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Updated"
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });


    }

    function uploadFile(image_type,type,input) {


        if (image_type == 'front_image' ) {

            if(input.files && input.files[0]){

                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#' + image_type)
                        .attr('src', e.target.result)
                        .height(60);
                };

                cc[image_type] = input.files[0];
                reader.readAsDataURL(input.files[0]);
                cc.upload_files_type += "F";
                cc.selected_catalog.new_front_image_url = "";

                $scope.$apply();
            }
            else{
                cc.upload_files_type.replace('F','');
                cc.selected_catalog.temp_front_image = "";
            }

        }

    }

    function uploadFilesOnServer() {

        ngDialog.close({
            template: 'updateCatalogDetailsModal',
            className: 'ngdialog-theme-default',
            showClose: true,
            scope: $scope
        });

        if(cc.upload_files_type.indexOf('F') < 0 && cc.selected_catalog.new_front_image_url.length <= 0){

            SweetAlert.swal({
                title: "",
                text: "Please select front image."
            },function () {
                ngDialog.open({
                    template: 'updateCatalogDetailsModal',
                    className: 'ngdialog-theme-default',
                    showClose: true,
                    scope: $scope
                });
            });

            return false;

        }


        if(cc.upload_files_type.length == 0){ //Already Uploaded on server
            cc.selected_catalog.send_front_image = cc.selected_catalog.new_front_image_url;
            cc.UpdateCatalogDetails();
        }
        else{

            cc.spinners     = true;

            var formData = new FormData();
            var multi_count = 0;
            var count = 0;

            if(cc.upload_files_type.indexOf('F') >= 0){
                formData.append('multi_img_' + count, cc.front_image);
                multi_count += 1;
                count += 1;
            }
            else{
                cc.selected_catalog.send_front_image = cc.selected_catalog.new_front_image_url;
            }

            formData.append('multi_count', multi_count);

            $.ajax({
                type: 'POST',
                // url: mainVm.urlList.order_url + 'file-upload-to-s3',
                url: mainVm.urlList.order_url + 'image-upload-to-cloudinary',
                dataType: "json",
                data: formData,
                async: true,
                processData: false,
                contentType: false,
                success: function (data) {

                    //Front Image
                    if(cc.upload_files_type.indexOf('F') >= 0){
                        cc.selected_catalog.send_front_image = data.data[0];
                    }

                    cc.upload_files_type = "";

                    cc.UpdateCatalogDetails();
                    $scope.$apply();

                },
                error: function (data) {
                    mainServices.checkErrorStatus(data.status);
                }
            });


        }

    }

    function openUpdateSizeModal(){

        cc.seleted_item = "";
        cc.new_size = "";
        cc.selected_items = [];

        if(cc.selected_catalog.id){
            cc.spinners     = true;

            $http({
                method: 'GET',
                url: mainVm.urlList.flymall2 + 'catalog/items/?style_details_id=' + cc.selected_catalog.id,
                headers: mainVm.headers
            }).success(function (response) {

                var res = mainServices.nodeCheckErrorStatus(response.status);

                if(!res){
                    return false;
                }

                cc.selected_catalog.item_info = response.data;
                cc.spinners     = false;

                ngDialog.open({
                    template: 'updateItemSize',
                    className: 'ngdialog-theme-default consignment-view-modal',
                    showClose: true,
                    scope: $scope
                });


            });
        }
        else{
            SweetAlert.swal({
                title: "",
                text: "Please select Catalog."
            });
        }

    }

    function updateItemSize() {

        cc.spinners     = true;

        ngDialog.close({
            template: 'updateItemSize',
            className: 'ngdialog-theme-default',
            showClose: true,
            scope: $scope
        });


        $http({
            method: 'PUT',
            url: mainVm.urlList.flymall + 'catalog/item-size/',
            data : {
                "id" : cc.seleted_item,
                "new_size" : cc.new_size
            },
            headers: mainVm.headers
        }).success(function (response) {

            var res = mainServices.nodeCheckErrorStatus(response.status);

            if(!res){
                return false;
            }

            cc.spinners = false;

            if(response.is_error){

                if(typeof(response.message) == 'string'){
                    SweetAlert.swal({
                        title: "",
                        text: response.message
                    });
                }
                else{
                    SweetAlert.swal({
                        title: "",
                        text: response.message[0]
                    });
                }

            }
            else{
                SweetAlert.swal({
                    title: "",
                    text: "Updated"
                });
            }


        })
            .error(function (response,status) {
                mainServices.checkErrorStatus(status);
            });


    }

}


angular
    .module('flyrobe')
    .controller('catalogCtrl', catalogCtrl);
