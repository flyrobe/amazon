angular
    .module('flyrobe')
    .filter('address', function () {
        return function (item) {
        };
    })
    .filter('consignment_status', function () {
        return function (item) {

            if(item == 'INI'){
                return 'Initiated';
            }
            else if(item == 'RTS'){
                return 'Ready to be shipped';
            }
            else if(item == 'ITR'){
                return 'In Transit';
            }
            else if(item == 'PRC'){
                return 'Partially Received';
            }
            else if(item == 'RCD'){
                return 'Received';
            }
            else{
                return item
            }

        };
    })
    .filter('itemStatus', function () {

        return function (item) {
            var list = {
                'PI'  : 'Initial',
                'AVL' : 'Live',
                'LIV' : 'Live',
                'STS' : 'Sent To Host',
                'DMG' : 'Damaged',
                'INT' : 'in transit',
                'SOT' : 'Sold Out'
            };

            if(list[item]){
                return list[item];
            }
            else{
                return item;
            }
        };



    })
    .filter('consignmentState', function () {
        return function (item) {
            var list = {
                'SNT': 'Sent To Host',
                'RCD': 'Received',
                'NRCD': 'Not Received',
                'RTN': 'Return',
                'SOT' : 'Sold Out'
            };

            if (list[item]) {
                return list[item];
            }
            else {
                return item;
            }
        };

    })

    .filter('tab', function () {
        return function (tab_Name) {

            if (mainVm.fly_amazon.user_role_based_access[0].resource_access_permissions[0].tabs.indexOf(tab_Name) >= 0) {
                return true;
            }
            else {
                return false;
            }

        };
    })
    .filter('component', function ($localStorage) {
        return function (component,tab) {

            if(tab && component){
                if(mainVm.fly_amazon.user_role_based_access[0].resource_access_permissions[0].panels[tab][component]){
                    return mainVm.fly_amazon.user_role_based_access[0].resource_access_permissions[0].panels[tab][component];
                }
                else{
                    return 'H';
                }
            }
            else{
                return 'H';
            }

        };
    })
;




